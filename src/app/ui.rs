pub mod elements;
pub mod menu;
pub mod panel_bottom;
pub mod panel_central;
pub mod panel_side;
pub mod panel_top;

use crate::app::{
    ui::{
        menu::UiMenu,
        panel_bottom::UiBottomPanel,
        panel_central::UiCentralPanel,
        panel_side::UiSidePanel,
        panel_top::UiTopPanel,
    },
    App,
};
use eframe::{
    egui::Context,
    Frame,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct UiApp {}

impl UiApp {
    pub fn draw_ui(
        app: &mut App,
        context: &Context,
        frame: &mut Frame,
    ) {
        // frame.info().system_theme.unwrap().egui_visuals();
        context
            .set_theme(
                app
                    .options
                    .borrow()
                    .theme,
            );
    // Menu:
        // println!("menu");
        UiMenu::draw(
            context,
            app
                .current_sign
                .clone(),
            app
                .dictionary
                .clone(),
            app
                .localization
                .clone(),
            app
                .options
                .clone(),
        );
    // Bottom panel:
        // println!("bottom panel");
        UiBottomPanel::draw(
            context,
            frame
                .info()
                .cpu_usage,
            // app
            //     .options
            //     .clone(),
        );
    // Side panel:
        // println!("side panel");
        UiSidePanel::draw(
            app
                .camera
                .clone(),
            context,
            app
                .current_sign
                .clone(),
            app
                .dictionary
                .clone(),
            app
                .localization
                .clone(),
            app
                .options
                .clone(),
        );
    // Top panel:
        // println!("top panel");
        UiTopPanel::draw(
            context,
            app
                .current_sign
                .clone(),
            app
                .current_step
                .clone(),
            app
                .dictionary
                .clone(),
            app
                .filter
                .clone(),
            app
                .localization
                .clone(),
            app
                .options
                .clone(),
        );
    // Central panel:
        // println!("central panel");
        UiCentralPanel::draw(
            app
                .camera
                .clone(),
            context,
            app
                .current_sign
                .clone(),
            app
                .current_step
                .clone(),
        );
    }

}
