animation = Animation
category = Catégorie
dark = Sombre
english = anglaise
filter = Filtre
for_easy_dictionary_editing_1  = pour une
for_easy_dictionary_editing_2  = édition facile
for_easy_dictionary_editing_3  = du dictionnaire
french = française
german = allemande
language = Langue
light = Clair
load_file = charger le fichier
mirror = refléter
mirrored = miroir
open_file = Ouvrir le Fichier
reload = recharger
reset = réinitialiser
restart = recommencer
select_a_sign = sélectionner un signe
sign_language = Langage des Signes
sign_not_available = signe non disponible
space = espace
system = Système
theme = Thème
view = Vue

app = Application
camera = Caméra
description = description
move_h = gche | droite
move_v = bas | haut
pause = pause
scale = échelle
settings = Configuration
unpause = continuer
