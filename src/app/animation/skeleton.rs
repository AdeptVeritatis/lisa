// #![allow(clippy::too_many_arguments)]

use crate::{
    app::animation::{
        body::{
            AnimationFinger,
            AnimationHand,
        },
        camera::Camera,
        shape::AnimationShape,
        vertices::{
            Rectangle,
            Vertex,
        },
        AnimationSide,
    },
    constants::{
        ARM_LOWER_LENGTH,
        ARM_UPPER_LENGTH,
        ARM_LOWER_COLOR,
        ARM_UPPER_COLOR,
        ARM_WIDTH,
        COLORS,
        FINGER_LENGTH,
        FINGER_WIDTH,
        FINGER_INDEX_LENGTH_FACTOR,
        FINGER_MIDDLE_LENGTH_FACTOR,
        FINGER_PINKIE_LENGTH_FACTOR,
        FINGER_RING_LENGTH_FACTOR,
        FINGER_THUMB_LENGTH_FACTOR,
        LIMB_BASE_COLOR,
        LIMB_MIDDLE_COLOR,
        LIMB_TIP_COLOR,
        LIMB_BASE_LENGTH_FACTOR,
        LIMB_MIDDLE_LENGTH_FACTOR,
        LIMB_TIP_LENGTH_FACTOR,
        PALM_COLOR,
        PALM_DEPTH,
        PALM_LENGTH,
        PALM_WIDTH,
        // RECTANGLE_PREV_ANGLE,
        START_X_L,
        START_X_R,
        START_Y,
        START_Z,
    },
};
use glam::{
    // Mat3,
    Quat,
    Vec3,
};
use std::f32::consts::FRAC_PI_2;

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct Skeleton {
    pub rectangles: Vec<Rectangle>,
}

impl Default for Skeleton {
    fn default() -> Self {
        Self {
            rectangles: Vec::new(),
        }
    }
}

impl Skeleton {
    pub fn new(
        camera: Camera,
        shape: AnimationShape,
    ) -> Self {
    // Create a skeleton model:
        let mut skeleton: Skeleton = Skeleton::default();
    // Left:
        // Create joints:
        // Arm upper left.
        let mut ball_joint: BallJoint = BallJoint {
            angles: shape
                .arm_left
                .upper,
            rotation: shape
                .arm_left
                .rotation,
            length: ARM_UPPER_LENGTH,
            width: ARM_WIDTH,
            color: COLORS[ARM_UPPER_COLOR],
        };
        // Arm lower left.
        let mut hinge_joint: HingeJoint = HingeJoint {
            angle: shape
                .arm_left
                .lower,
            length: ARM_LOWER_LENGTH,
            width: ARM_WIDTH,
            color: COLORS[ARM_LOWER_COLOR],
        };

        // Create limbs and rectangles:
        let arm_upper_left: Limb =
            Limb::start(
                camera
                    .mirrored,
                AnimationSide::Left,
                Vec3::new(
                    match
                        camera
                            .mirrored
                    {
                        true => {
                            - START_X_L + camera.move_h
                        },
                        false => {
                            START_X_L + camera.move_h
                        },
                    },
                    START_Y + camera.move_v,
                    START_Z,
                ),
            )
            .next_from_ball_joint(
                ball_joint,
            );
        skeleton
            .rectangles
            .push(
                Rectangle::from_limb(
                    arm_upper_left,
                )
            );
        let arm_lower_left: Limb = arm_upper_left
            .next_from_hinge_joint(
                hinge_joint,
            );
        skeleton
            .rectangles
            .push(
                Rectangle::from_limb(
                    arm_lower_left
                )
            );
        // Hand left.
        skeleton = skeleton
            .add_hand(
                arm_lower_left,
                shape
                    .hand_left,
            );

    // Right:
        // Create joints:
        // Arm upper right.
        ball_joint = BallJoint {
            angles: shape
                .arm_right
                .upper,
            rotation: shape
                .arm_right
                .rotation,
            length: ARM_UPPER_LENGTH,
            width: ARM_WIDTH,
            color: COLORS[ARM_UPPER_COLOR],
        };
        // Arm lower right.
        hinge_joint = HingeJoint {
            angle: shape
                .arm_right
                .lower,
            length: ARM_LOWER_LENGTH,
            width: ARM_WIDTH,
            color: COLORS[ARM_LOWER_COLOR],
        };

        // Create limbs and rectangles:
        let arm_upper_right: Limb =
            Limb::start(
                camera
                    .mirrored,
                AnimationSide::Right,
                Vec3::new(
                    match
                        camera
                            .mirrored
                    {
                        true => {
                            - START_X_R + camera.move_h
                        },
                        false => {
                            START_X_R + camera.move_h
                        },
                    },
                    START_Y + camera.move_v,
                    START_Z,
                ),
            )
            .next_from_ball_joint(
                ball_joint,
            );
        skeleton
            .rectangles
            .push(
                Rectangle::from_limb(
                    arm_upper_right
                )
             );
        let arm_lower_right: Limb = arm_upper_right
            .next_from_hinge_joint(
                hinge_joint,
            );
        skeleton
            .rectangles
            .push(
                Rectangle::from_limb(
                    arm_lower_right
                )
            );
        // Hand right.
        skeleton = skeleton
            .add_hand(
                arm_lower_right,
                shape
                    .hand_right,
            );

        skeleton
    }
}

impl Skeleton {
    fn add_hand(
        &mut self,
        limb: Limb,
        shape: AnimationHand,
    ) -> Self {
        Hand::add(
            limb,
            shape,
            self.to_owned(),
        )
    }

    pub fn get_vertices(
        &self,
    ) -> Vec<Vertex> {
        let mut vertices: Vec<Vertex> = Vec::new();
        for
            rectangle
        in
            self
                .rectangles
                .clone()
        {
            vertices = rectangle
                .create_vertices(
                    vertices,
                );
        }

        vertices
    }

}

// ----------------------------------------------------------------------------

/// Rectangle in orientation of the xy-plane to guarantee visibility from camera starting position.
#[derive(Clone, Copy)]
pub struct Limb {
    pub axis_x: Vec3,
    pub axis_y: Vec3,
    pub axis_z: Vec3,
    pub color: [f32; 4],
    pub end: Vec3,
    pub length_vector: Vec3,
    pub side: AnimationSide,
    pub start: Vec3,
    pub width: Vec3,
    pub width_vector: Vec3,
}

impl Limb {
    pub fn start(
        mirrored: bool,
        mut side: AnimationSide,
        start: Vec3,
    ) ->  Self {
        side = match
            mirrored
        {
            false => side,
            true => match
                side
            {
                AnimationSide::Left => AnimationSide::Right,
                AnimationSide::Right => AnimationSide::Left,
            },
        };

        // Fingers are directed to floor.
        let length_vector: Vec3 = Vec3::NEG_Y;
        // Thumb is directed to the outside of the body.
        // Palm faces forwards.
        let width_vector: Vec3 = match
            side
        {
            AnimationSide::Left => Vec3::X,
            AnimationSide::Right => Vec3::NEG_X,
        };

        // Rotation axes.
        let axis_x: Vec3 = Vec3::X;
        let axis_y: Vec3 = Vec3::Y;
        let axis_z: Vec3 = Vec3::Z;

        // default values???
        let color: [f32; 4] = COLORS[0];
        let width: Vec3 = Vec3 {
            x: 0.1, // ???
            y: 0.0,
            z: 0.0,
        };

        Self {
            axis_x,
            axis_y,
            axis_z,
            color,
            // Set end to start, because end will be next start.
            end: start,
            length_vector,
            side,
            start,
            width,
            width_vector,
        }
    }

    pub fn next_from_ball_joint(
        &self,
        ball_joint: BallJoint,
    ) -> Self {
        let angles: [f32; 3] = ball_joint
            .angles;
        let length_factor: f32 = ball_joint
            .length;
        let rotation: f32 = ball_joint
            .rotation;
        let width_factor: f32 = ball_joint
            .width;

        // Data from previous limb:
        let mut axis_x: Vec3 = self
            .axis_x;
        let mut axis_y: Vec3 = self
            .axis_y;
        let mut axis_z: Vec3 = self
            .axis_z;
        let mut length_vector: Vec3 = self
            .length_vector;
        let side: AnimationSide = self
            .side;
        let start: Vec3 = self
            .end;
        let mut width_vector: Vec3 = self
            .width_vector;

    // Calculate rotations:
        // Rotates all axes around y-axis.
        // Is side dependent.
        let rotation_y: Quat = Quat::from_axis_angle(
            axis_y,
            match
                side
            {
                AnimationSide::Left => - angles[1],
                AnimationSide::Right => angles[1],
            }
        );
        width_vector = rotation_y * width_vector;
        axis_x = rotation_y * axis_x;
        axis_z = rotation_y * axis_z;

        // Rotates around x-axis.
        // Is not side dependent.
        let rotation_x: Quat = Quat::from_axis_angle(
            axis_x,
            - angles[0],
        );
        length_vector = rotation_x * length_vector;
        width_vector = rotation_x * width_vector;
        axis_y = rotation_x * axis_y;
        axis_z = rotation_x * axis_z;

        // Rotates around z-axis.
        // Is side dependent.
        let rotation_z: Quat = Quat::from_axis_angle(
            axis_z,
            match
                side
            {
                AnimationSide::Left => angles[2],
                AnimationSide::Right => - angles[2],
            },
        );
        length_vector = rotation_z * length_vector;
        width_vector = rotation_z * width_vector;
        axis_x = rotation_z * axis_x;
        axis_y = rotation_z * axis_y;

        // Rotates around y-axis.
        // Is side dependent.
        let rotation_rot: Quat = Quat::from_axis_angle(
            length_vector,
            match
                side
            {
                AnimationSide::Left => rotation,
                AnimationSide::Right => - rotation,
            }
        );
        width_vector = rotation_rot * width_vector;
        axis_x = rotation_rot * axis_x;
        axis_y = rotation_rot * axis_y;
        axis_z = rotation_rot * axis_z;

    // Calculate end point:
        let end: Vec3 = start + length_factor * length_vector;

    // Calculate width of rectangle:
        let width: Vec3 = calculate_width(
            length_vector,
            width_factor,
        );
    // Return:
        Self {
            axis_x,
            axis_y,
            axis_z,
            color: ball_joint
                .color,
            end,
            length_vector,
            side,
            start,
            width,
            width_vector,
        }
    }

    pub fn next_from_hinge_joint(
        &self,
        hinge_joint: HingeJoint,
    ) -> Self {
        let angle: f32 = hinge_joint
            .angle;
        let length_factor: f32 = hinge_joint
            .length;
        let width_factor: f32 = hinge_joint
            .width;

        // Data from previous limb:
        let mut axis_x: Vec3 = self
            .axis_x;
        let mut axis_y: Vec3 = self
            .axis_y;
        let mut axis_z: Vec3 = self
            .axis_z;
        let mut length_vector: Vec3 = self
            .length_vector;
        let side: AnimationSide = self
            .side;
        let start: Vec3 = self
            .end;
        let width_vector: Vec3 = self
            .width_vector;

    // Calculate rotations:
        // Rotates around width vector.
        // Is not side dependent. ???
        let rotation_hinge: Quat = Quat::from_axis_angle(
            width_vector,
            match
                side
            {
                AnimationSide::Left => - angle,
                AnimationSide::Right => angle,
            },
        );
        length_vector = rotation_hinge * length_vector;
        axis_x = rotation_hinge * axis_x;
        axis_y = rotation_hinge * axis_y;
        axis_z = rotation_hinge * axis_z;

    // Calculate end point:
        let end: Vec3 = start + length_factor * length_vector;

    // Calculate width of rectangle:
        let width: Vec3 = calculate_width(
            length_vector,
            width_factor,
        );

    // Return:
        Self {
            axis_x,
            axis_y,
            axis_z,
            color: hinge_joint
                .color,
            end,
            length_vector,
            side,
            start,
            width,
            width_vector,
        }
    }

}

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct Hand {
}

impl Hand {
    pub fn add( // fn new() ???
        arm_limb: Limb,
        shape: AnimationHand,
        mut skeleton: Skeleton,
    ) -> Skeleton {
        const FINGER_DIFF: f32 = 0.25; // * 2.0 / 8.0; // 2 * width and 4 fingers with 2 diffs
        // const PALM_DIFF: f32 = PALM_WIDTH - PALM_DEPTH;

    // Palm limb:
        let palm_width: f32 = PALM_DEPTH; // not correct!!!
        // let palm_width: f32 = PALM_DIFF * width_vector.x + PALM_DEPTH; // not correct!!!
        // let palm_width: f32 = PALM_DIFF * angle_rot.cos().abs() + PALM_DEPTH; // not correct in some cases!!!

        let mut ball_joint: BallJoint = BallJoint {
            angles: shape
                .palm,
            rotation: shape
                .rotation,
            length: PALM_LENGTH,
            width: palm_width,
            color: COLORS[PALM_COLOR],
        };
        let palm_limb: Limb = arm_limb
            .next_from_ball_joint(
                ball_joint,
            );
        skeleton
            .rectangles
            .push(
                Rectangle::from_limb(
                    palm_limb
                )
            );

        let palm_vector: Vec3 = PALM_WIDTH * palm_limb.width_vector;

    // Palm rectangle:
        // Rectangle in orientation of the finger plane to better see the posture of the hand.
        let rectangle_palm: Rectangle = Rectangle {
            color: COLORS[PALM_COLOR],
            start: palm_limb
                .start,
            end: palm_limb
                .end,
            width: palm_vector,
        };
        skeleton
            .rectangles
            .push(
                rectangle_palm
            );

    // Finger:
        let index_finger: Finger = Finger {
            animation: shape
                .index,
            length: FINGER_INDEX_LENGTH_FACTOR,
            position: 3.0,
        };
        let middle_finger: Finger = Finger {
            animation: shape
                .middle,
            length: FINGER_MIDDLE_LENGTH_FACTOR,
            position: 1.0,
        };
        let ring_finger: Finger = Finger {
            animation: shape
                .ring,
            length: FINGER_RING_LENGTH_FACTOR,
            position: -1.0,
        };
        let pinkie_finger: Finger = Finger {
            animation: shape
                .pinkie,
            length: FINGER_PINKIE_LENGTH_FACTOR,
            position: -3.0,
        };

        let fingers: [Finger; 4] = [
            index_finger,
            middle_finger,
            ring_finger,
            pinkie_finger,
        ];


        for
            finger
        in
            fingers
        {
            // Create joints:
            ball_joint = BallJoint {
                angles: finger
                    .animation
                    .base,
                rotation: finger
                    .animation
                    .rotation,
                length: FINGER_LENGTH * LIMB_BASE_LENGTH_FACTOR * finger.length,
                width: FINGER_WIDTH,
                color: COLORS[LIMB_BASE_COLOR],
            };
            let finger_middle: HingeJoint = HingeJoint {
                angle: finger
                    .animation
                    .middle,
                length: FINGER_LENGTH * LIMB_MIDDLE_LENGTH_FACTOR * finger.length,
                width: FINGER_WIDTH,
                color: COLORS[LIMB_MIDDLE_COLOR],
            };
            let finger_tip: HingeJoint = HingeJoint {
                angle: finger
                    .animation
                    .tip,
                length: FINGER_LENGTH * LIMB_TIP_LENGTH_FACTOR * finger.length,
                width: FINGER_WIDTH,
                color: COLORS[LIMB_TIP_COLOR],
            };
            let hinges: [HingeJoint; 2] = [
                finger_middle,
                finger_tip,
            ];

            // (Re)set values:
            let mut finger_limb: Limb = palm_limb;
            // Set starting position of finger:
            finger_limb.end += FINGER_DIFF * finger.position * palm_vector;

            // Create limbs and rectangles:
            finger_limb = finger_limb
                .next_from_ball_joint(
                    ball_joint,
                );
            skeleton
                .rectangles
                .push(
                    Rectangle::from_limb(
                        finger_limb
                    )
                );
            for
                hinge
            in
                hinges
            {
                finger_limb = finger_limb
                    .next_from_hinge_joint(
                        hinge,
                    );
                skeleton
                    .rectangles
                    .push(
                        Rectangle::from_limb(
                            finger_limb
                        )
                    );
            };
        };

    // Thumb:
        // Create joints:
        ball_joint = BallJoint {
            angles: shape
                .thumb
                .base,
            rotation: shape
                .thumb
                .rotation,
            length: FINGER_LENGTH * LIMB_BASE_LENGTH_FACTOR * FINGER_THUMB_LENGTH_FACTOR,
            width: FINGER_WIDTH,
            color: COLORS[LIMB_BASE_COLOR],
        };
        let thumb_middle: HingeJoint = HingeJoint {
            angle: shape
                .thumb
                .middle,
            length: FINGER_LENGTH * LIMB_MIDDLE_LENGTH_FACTOR * FINGER_THUMB_LENGTH_FACTOR,
            width: FINGER_WIDTH,
            color: COLORS[LIMB_MIDDLE_COLOR],
        };
        let thumb_tip: HingeJoint = HingeJoint {
            angle: shape
                .thumb
                .tip,
            length: FINGER_LENGTH * LIMB_TIP_LENGTH_FACTOR * FINGER_THUMB_LENGTH_FACTOR,
            width: FINGER_WIDTH,
            color: COLORS[LIMB_TIP_COLOR],
        };
        let hinges: [HingeJoint; 2] = [
            thumb_middle,
            thumb_tip,
        ];

        // Set values:
        let mut thumb_limb: Limb = palm_limb;

        // Set starting position of thumb:
        thumb_limb.end = thumb_limb.start + palm_vector;

        // Thumb folds parallel to palm.
        // Rotate width vector for thumb.
        // Ignore other vectors. They are not needed anymore. Maybe different solution???
        let rotation_rot: Quat = Quat::from_axis_angle(
            thumb_limb
                .length_vector,
            match
                thumb_limb
                    .side
            {
                AnimationSide::Left => FRAC_PI_2,
                AnimationSide::Right => - FRAC_PI_2,
            }
        );
        thumb_limb.axis_x = rotation_rot * thumb_limb.axis_x;
        thumb_limb.axis_z = rotation_rot * thumb_limb.axis_z;
        thumb_limb.width_vector = rotation_rot * thumb_limb.width_vector;

        // Create limbs and rectangles:
        thumb_limb = thumb_limb
            .next_from_ball_joint(
                ball_joint,
            );
        skeleton
            .rectangles
            .push(
                Rectangle::from_limb(
                    thumb_limb
                )
            );

        for
            hinge
        in
            hinges
        {
            thumb_limb = thumb_limb
                .next_from_hinge_joint(
                    hinge,
                );

            skeleton
                .rectangles
                .push(
                    Rectangle::from_limb(
                        thumb_limb
                    )
                );
        };

        skeleton
    }

}

// ----------------------------------------------------------------------------

struct Finger {
    animation: AnimationFinger,
    length: f32,
    position: f32,
}

// ----------------------------------------------------------------------------

pub struct BallJoint { // pub???
    pub angles: [f32; 3],
    pub rotation: f32,
    pub length: f32,
    pub width: f32,
    pub color: [f32; 4],
}

// ----------------------------------------------------------------------------

pub struct HingeJoint { // pub???
    pub angle: f32,
    pub length: f32,
    pub width: f32,
    pub color: [f32; 4],
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn calculate_width(
    length_vector: Vec3,
    width_factor: f32,
) -> Vec3 {
    // Fails, when angle_yz(?) is 0.5 * PI !!!???!!!

    // Width is not dependent on rotation around x-axis.
    // Width is dependent on rotation around z-axis.
    // Cos of x-value is not side dependent. It is symmetrical to 0.
    // Y-value is not side dependent.
    // Z-value is always zero.
    // length.project_onto_normalized(Vec3::X); // ???
    let mut corrected_angle_xy: f32 = length_vector
        .with_z(0.0)
        .angle_between(
            Vec3::NEG_Y
        );
    if
        length_vector
            .x
        <
        0.0
    {
        corrected_angle_xy = - corrected_angle_xy; // ???
    };
    // let mut corrected_angle_yz: f32 = length_vector
    //     .with_x(0.0)
    //     .angle_between(
    //         Vec3::NEG_Y
    //     );
    // if
    //     length_vector
    //         .z
    //     < // ???
    //     0.0
    // {
    //     corrected_angle_yz = - corrected_angle_yz; // ???
    // };
    Vec3::new(
        width_factor * corrected_angle_xy.cos(),
        width_factor * corrected_angle_xy.sin(), // ???
        0.0,
    )
}
