struct VertexOut {
    @location(0) color: vec4<f32>,
    @builtin(position) position: vec4<f32>,
};

struct Data {
    // from camera to screen
    proj: mat4x4<f32>,
    // from screen to camera
    // proj_inv: mat4x4<f32>,
    // from world to camera
    view: mat4x4<f32>,
    // camera position
    // cam_pos: vec4<f32>,
};

@group(0)
@binding(0)
var<uniform> r_data: Data;

@vertex
fn vs_main(
    @location(0) v_color: vec4<f32>,
    @location(1) v_pos: vec4<f32>,
) -> VertexOut {
    var out: VertexOut;
    // out.view = v_pos - r_data.cam_pos.xyz;
    out.position = r_data.proj * r_data.view * v_pos;

    // out.position = v_pos;
    out.color = v_color;

    return out;
}

@fragment
fn fs_main(
    in: VertexOut,
) -> @location(0) vec4<f32> {
    return in.color;
}
