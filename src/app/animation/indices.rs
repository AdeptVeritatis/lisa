
// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn create_indices(
    len: usize,
) -> Vec<u16> {
// All indices:
    // 1 upper arm + 1 lower arm + 1 hand palm = 3 limbs
    // 1 finger base + 1 finger middle + 1 finger tip = 3 limbs
    // 4 (2 * arm + 2 * palm) + 5 * 3 (finger) = 19 limbs
    // 19 limbs  * 2 sides = 38 limbs
    // 38 lims * 2 triangles
    // 38 limbs * 4 vertices = 152

    let repetitions: usize = len / 4; // rounding ???

    let mut indices: Vec<u16> = Vec::new();
    for
        i_num
    in
        0..repetitions
    {
        indices
            .push(
                i_num as u16 * 4
            );
        indices
            .push(
                i_num as u16 * 4 + 1
            );
        indices
            .push(
                i_num as u16 * 4 + 2
            );
        indices
            .push(
                i_num as u16 * 4 + 1
            );
        indices
            .push(
                i_num as u16 * 4 + 2
            );
        indices
            .push(
                i_num as u16 * 4 + 3
            );
    };

    indices
}
