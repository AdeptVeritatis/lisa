
use crate::{
    app::animation::{
        camera::Camera,
        indices::create_indices,
        shape::AnimationShape,
        skeleton::Skeleton,
        vertices::Vertex,
    },
    constants::{
        DEPTH_FORMAT,
        SHADER,
    },
};
use eframe::{
    egui::PaintCallbackInfo,
    egui_wgpu::{
        CallbackResources,
        CallbackTrait,
        RenderState,
        ScreenDescriptor,
    },
    wgpu::{
        util::{
            BufferInitDescriptor,
            DeviceExt,
        },
        BindGroupLayoutDescriptor,
        BindGroupLayoutEntry,
        BindingType,
        BindGroup,
        BindGroupDescriptor,
        BindGroupEntry,
        BindGroupLayout,
        Buffer,
        BufferAddress,
        BufferBindingType,
        // BufferSize,
        BufferUsages,
        CommandBuffer,
        CommandEncoder,
        CompareFunction,
        Device,
        DepthBiasState,
        DepthStencilState,
        FragmentState,
        IndexFormat,
        MultisampleState,
        PipelineCompilationOptions,
        PipelineLayout,
        PipelineLayoutDescriptor,
        PrimitiveState,
        Queue,
        RenderPass,
        RenderPipeline,
        RenderPipelineDescriptor,
        ShaderModule,
        ShaderModuleDescriptor,
        ShaderSource,
        ShaderStages,
        StencilState,
        VertexAttribute,
        VertexBufferLayout,
        VertexFormat,
        VertexState,
        VertexStepMode,
    },
    // CreationContext,
    // Frame,
};
use std::{
    borrow::Cow,
    sync::Arc,
};

// ----------------------------------------------------------------------------

pub struct BodyModelCallback {
    pub animation_shape: AnimationShape,
    pub camera: Camera,
    pub note: String,
}

impl CallbackTrait for BodyModelCallback {
    fn prepare(
        &self,
        device: &Device,
        queue: &Queue,
        _descriptor: &ScreenDescriptor,
        _egui_encoder: &mut CommandEncoder,
        resources: &mut CallbackResources,
    ) -> Vec<CommandBuffer> {
        // Update vertex buffer:
        let resources: &BodyModelRenderResources = resources
            .get()
            .unwrap();
        resources
            .prepare(
                self
                    .animation_shape
                    .clone(),
                self
                    .camera,
                device,
                queue,
            );
        Vec::new()
    }

    fn paint(
        &self,
        _info: PaintCallbackInfo,
        render_pass: &mut RenderPass<'static>,
        resources: &CallbackResources,
    ) {
        let resources: &BodyModelRenderResources = resources
            .get()
            .unwrap();
        resources
            .paint(
                render_pass
            );
    }
}

// ----------------------------------------------------------------------------

pub struct BodyModelRenderResources {
    pub bind_group: BindGroup,
    pub index_buffer: Buffer,
    pub index_count: usize,
    pub pipeline: RenderPipeline,
    pub uniform_buffer: Buffer,
    pub vertex_buffer: Buffer,
}

impl BodyModelRenderResources {
    fn prepare(
        &self,
        animation_shape: AnimationShape,
        camera: Camera,
        _device: &Device,
        queue: &Queue,
    ) {
        let vertices: Vec<Vertex> =
            Skeleton::new(
                camera,
                animation_shape
                    .to_owned(),
            )
            .get_vertices();
        queue
            .write_buffer(
                &self
                    .vertex_buffer,
                0,
                bytemuck::cast_slice(
                    vertices
                        .as_slice()
                ),
            );
        // Update our uniform buffer with the angle from the UI
        let raw_uniforms: [f32; 16 * 2 + 1] = camera
            .to_uniform_data();
        queue
            .write_buffer(
                &self
                    .uniform_buffer,
                0,
                bytemuck::cast_slice(
                    &raw_uniforms
                ),
            );
    }

    fn paint(
        &self,
        render_pass: &mut RenderPass<'_>,
    ) {
        render_pass
            .set_pipeline(
                &self
                    .pipeline
            );
        render_pass
            .set_index_buffer(
                self
                    .index_buffer
                    .slice(..),
                IndexFormat::Uint16,
            );
        render_pass
            .set_vertex_buffer(
                0,
                self
                    .vertex_buffer
                    .slice(..),
            );

        // render_pass.pop_debug_group();
        // render_pass.insert_debug_marker("Draw!");

        render_pass
            .set_bind_group(
                0,
                &self
                    .bind_group,
                &[],
            );
        render_pass
            .draw_indexed(
                0
                ..
                self
                    .index_count as u32,
                0,
                0..1,
            );
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

// First frame has wrong aspect ratio!!!
pub fn prepare_render_pipeline(
    camera: Camera,
    wgpu_render_state: &RenderState,
) {
    let device: &Arc<Device> = &wgpu_render_state
        .device;

    // Vertices:
    let vertex_size: usize = std::mem::size_of::<Vertex>();
    let vertices: Vec<Vertex> =
        Skeleton::new(
            camera,
            AnimationShape::example(), // default(),
        )
        .get_vertices();
    // Indices:
    let indices: Vec<u16> = create_indices(
        vertices
            .len(),
    );
    // Prepare camera for uniform buffer:
    let raw_uniforms: [f32; 16 * 2 + 1] = camera
        .to_uniform_data();

    // Buffer:
    let vertex_buffer: Buffer = device
        .create_buffer_init(
            &BufferInitDescriptor {
                label: Some("vertex_buffer"),
                contents: bytemuck::cast_slice(
                    &vertices
                ),
                usage: BufferUsages::VERTEX | BufferUsages::COPY_DST,
            }
        );
    let index_buffer: Buffer = device
        .create_buffer_init(
            &BufferInitDescriptor {
                label: Some("index_buffer"),
                contents: bytemuck::cast_slice(
                    &indices
                ),
                usage: BufferUsages::INDEX,
            }
        );
    let uniform_buffer: Buffer = device
        .create_buffer_init(
            &BufferInitDescriptor {
                label: Some("uniform_buffer"),
                contents: bytemuck::cast_slice(
                    &raw_uniforms
                ),
                usage: BufferUsages::UNIFORM | BufferUsages::COPY_DST,
            }
        );

    // Load shader from disk:
    let shader: ShaderModule = device
        .create_shader_module(
            ShaderModuleDescriptor {
                label: Some("body_model"),
                source: ShaderSource::Wgsl(
                    Cow::Borrowed(
                        // include_str!("app/render/shader.wgsl")
                        SHADER
                    )
                ),
            }
        );

    // Vertex buffer layout:
    let vertex_buffer_layout: VertexBufferLayout = VertexBufferLayout {
        array_stride: vertex_size as BufferAddress,
        step_mode: VertexStepMode::Vertex,
        attributes: &[
            VertexAttribute {
                format: VertexFormat::Float32x4,
                offset: 0,
                shader_location: 0,
            },
            VertexAttribute {
                format: VertexFormat::Float32x4,
                offset: 4 * 4,
                shader_location: 1,
            },
        ],
    };

    // Pipeline layout:
    let bind_group_layout: BindGroupLayout = device
        .create_bind_group_layout(
            &BindGroupLayoutDescriptor {
                label: Some("body_model"),
                entries: &[
                    BindGroupLayoutEntry {
                        binding: 0,
                        visibility: ShaderStages::VERTEX,
                        ty: BindingType::Buffer {
                            ty: BufferBindingType::Uniform,
                            has_dynamic_offset: false,
                            // min_binding_size: NonZeroU64::new(16),
                            // min_binding_size: BufferSize::new(64),
                            min_binding_size: None,
                        },
                        count: None,
                    },
                ],
            }
        );
    let pipeline_layout: PipelineLayout = device
        .create_pipeline_layout(
            &PipelineLayoutDescriptor {
                label: Some("body_model"),
                bind_group_layouts: &[
                    &bind_group_layout,
                ],
                push_constant_ranges: &[],
            }
        );

    // Create bind group:
    let bind_group: BindGroup = device
        .create_bind_group(
            &BindGroupDescriptor {
                layout: &bind_group_layout,
                entries: &[
                    BindGroupEntry {
                        binding: 0,
                        resource: uniform_buffer
                            .as_entire_binding(),
                    },
                ],
                label: None,
            }
        );

    // Pipeline:
    let pipeline: RenderPipeline  = device
        .create_render_pipeline(
            &RenderPipelineDescriptor {
                label: Some("body_model"),
                layout: Some(&pipeline_layout),
                cache: None, // ??????????????????????
                vertex: VertexState {
                    module: &shader,
                    entry_point: "vs_main",
                    compilation_options: PipelineCompilationOptions::default(),
                    buffers: &[
                        vertex_buffer_layout,
                    ],
                },
                fragment: Some(
                    FragmentState {
                        module: &shader,
                        entry_point: "fs_main",
                        compilation_options: PipelineCompilationOptions::default(),
                        targets: &[
                            Some(
                                wgpu_render_state
                                    .target_format
                                    .into()
                            ),
                        ],
                    }
                ),
                primitive: PrimitiveState::default(),
                depth_stencil: Some(
                    DepthStencilState {
                        format: DEPTH_FORMAT,
                        depth_write_enabled: true,
                        depth_compare: CompareFunction::LessEqual,
                        stencil: StencilState::default(),
                        bias: DepthBiasState::default(),
                    }
                ),
                multisample: MultisampleState::default(),
                multiview: None,
            }
        );

    wgpu_render_state
        .renderer
        .write()
        .callback_resources
        .insert(
            BodyModelRenderResources {
                bind_group,
                index_buffer,
                index_count: indices.len(),
                pipeline,
                uniform_buffer,
                vertex_buffer,
            }
        );
}
