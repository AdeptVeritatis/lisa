
use std::f32::consts::PI;
use toml::Value;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, PartialEq)]
// Arm has two limbs.
pub struct AnimationArm {
    // Upper arm has three rotations.
    pub upper: [f32; 3],
    pub rotation: f32,
    // Forearm has one axis.
    pub lower: f32,
}

impl Default for AnimationArm {
    fn default() -> Self {
        Self {
            upper: [0.0; 3],
            rotation: 0.0,
            lower: 0.0,
        }
    }
}

impl AnimationArm {
    pub fn example() -> Self {
        Self {
            upper: [
                0.30 * PI,
                0.0,
                0.15 * PI,
            ],
            rotation: 0.50 * PI,
            lower: 0.65 * PI,
        }
    }
}

impl AnimationArm {
    pub fn get_lower_arm(
        &mut self,
        arm_array: &[Value],
    ) {
        match
            arm_array
                .len()
        {
            0 => {
                println!("!! no value in lower arm array");
            },
            1 => {
                match
                    arm_array[0]
                        .as_float()
                {
                    None => {
                        println!("!! arm value is not a float");
                    },
                    Some(arm_float) => {
                        self.lower = arm_float as f32 * PI;
                    },
                };
            },
            _ => {
                println!("!! more than one value in lower arm array");
            },
        };
    }

    pub fn get_upper_arm(
        &mut self,
        arm_array: &[Value],
    ) {
        for
            (num, arm_value)
        in
            arm_array
                .iter()
                .enumerate()
        {
            match
                arm_value
                    .as_float()
            {
                None => {
                    println!("!! arm value is not a float");
                },
                Some(arm_float) => match
                    num
                {
                    0 => {
                        self.upper[num] = arm_float as f32 * PI;
                    },
                    1 => {
                        self.upper[num] = arm_float as f32 * PI;
                    },
                    2 => {
                        self.upper[num] = arm_float as f32 * PI;
                    },
                    3 => {
                        self.rotation = arm_float as f32 * PI;
                    },
                    _ => {
                        println!("!! too many values in upper arm array");
                    },
                },
            };
        };
    }

}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, PartialEq)]
// Hand with palm and five fingers.
// Palm has two axes and a rotation.
pub struct AnimationHand {
    pub palm: [f32; 3],
    pub rotation: f32,
    pub thumb: AnimationFinger,
    pub index: AnimationFinger,
    pub middle: AnimationFinger,
    pub ring: AnimationFinger,
    pub pinkie: AnimationFinger,
}

impl Default for AnimationHand {
    fn default() -> Self {
        Self {
            palm: [0.0; 3],
            rotation: 0.0,
            thumb: AnimationFinger::default(),
            index: AnimationFinger::default(),
            middle: AnimationFinger::default(),
            ring: AnimationFinger::default(),
            pinkie: AnimationFinger::default(),
        }
    }
}

impl AnimationHand {
    pub fn example() -> Self {
        Self {
            palm: [
                0.39 * PI,
                0.0,
                0.25 * PI,
            ],
            rotation: 0.90 * PI,
            thumb: AnimationFinger::example(),
            index: AnimationFinger::example(),
            middle: AnimationFinger::example(),
            ring: AnimationFinger::example(),
            pinkie: AnimationFinger::example(),
        }
    }
}

impl AnimationHand {
    pub fn get_palm(
        &mut self,
        palm_array: &[Value],
    ) {
        for
            (num, palm_value)
        in
            palm_array
                .iter()
                .enumerate()
        {
            match
                palm_value
                    .as_float()
            {
                None => {
                    println!("!! palm element is not a float");
                },
                Some(palm_float) => {
                    match
                        num
                    {
                        0 => {
                            self.palm[num] = palm_float as f32 * PI;
                        },
                        1 => {
                            self.palm[num] = palm_float as f32 * PI;
                        },
                        2 => {
                            self.palm[num] = palm_float as f32 * PI;
                        },
                        3 => {
                            self.rotation = palm_float as f32 * PI;
                        },
                        _ => {
                            println!("!! too many values in palm array");
                        },
                    }
                },
            };
        };
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, PartialEq)]
// Finger has three segments.
// Finger segments have two axes.
pub struct AnimationFinger {
    pub base: [f32; 3],
    pub rotation: f32,
    pub middle: f32,
    pub tip: f32,
}

impl Default for AnimationFinger {
    fn default() -> Self {
        Self {
            base: [0.0; 3],
            rotation:   0.0,
            middle:     0.0,
            tip:        0.0,
        }
    }
}

impl AnimationFinger {
    pub fn example() -> Self {
        Self {
            base: [
                -0.05 * PI,
                0.0,
                0.0,
            ],
            rotation:   0.0,
            middle:     0.03 * PI,
            tip:        0.05 * PI,
        }
    }
}

impl AnimationFinger {
    pub fn get_finger(
        &mut self,
        finger_array: &[Value],
    ) {
        for
            (num, finger_value)
        in
            finger_array
                .iter()
                .enumerate()
        {
            match
                finger_value
                    .as_float()
            {
                None => {
                    println!("!! finger element is not a float");
                },
                Some(finger_float) => {
                    match
                        num
                    {
                        0 => {
                            self.base[num] = finger_float as f32 * PI;
                        },
                        1 => {
                            self.base[num] = finger_float as f32 * PI;
                        },
                        2 => {
                            self.base[num] = finger_float as f32 * PI;
                        },
                        3 => {
                            self.rotation = finger_float as f32 * PI;
                        },
                        4 => {
                            self.middle = finger_float as f32 * PI;
                        },
                        5 => {
                            self.tip = finger_float as f32 * PI;
                        },
                        _ => {
                            println!("!! finger array has too many elements");
                        },
                    };
                },
            };
        };
    }
}

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
// pub struct AnimationMouth {
//     pub upper: [f32; 2],
// }

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
