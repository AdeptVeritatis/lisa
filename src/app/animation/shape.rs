
use crate::app::animation::{
    body::{
        AnimationArm,
        AnimationFinger,
        AnimationHand,
    },
    AnimationValues,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, PartialEq)]
pub struct AnimationShape {
    pub arm_left: AnimationArm,
    pub arm_right: AnimationArm,
    pub hand_left: AnimationHand,
    pub hand_right: AnimationHand,
    // pub mouth: AnimationMouth,

    pub animation: Option<AnimationValues>,
}

impl Default for AnimationShape {
    fn default() -> Self {
        Self {
            arm_left: AnimationArm::default(),
            arm_right: AnimationArm::default(),
            hand_left: AnimationHand::default(),
            hand_right: AnimationHand::default(),

            animation: None,
        }
    }
}

impl AnimationShape {
    pub fn example() -> Self {
        Self {
            arm_left: AnimationArm::example(),
            arm_right: AnimationArm::example(),
            hand_left: AnimationHand::example(),
            hand_right: AnimationHand::example(),

            animation: None,
        }
    }
}

impl AnimationShape {
    pub fn current_step(
        diff_factor: f32,
        shape_current: AnimationShape,
        shape_diff: AnimationShape,
    ) -> Self {
        Self {
            arm_left: AnimationArm {
                upper: [
                    shape_current.arm_left.upper[0] + shape_diff.arm_left.upper[0] * diff_factor,
                    shape_current.arm_left.upper[1] + shape_diff.arm_left.upper[1] * diff_factor,
                    shape_current.arm_left.upper[2] + shape_diff.arm_left.upper[2] * diff_factor,
                ],
                rotation: shape_current.arm_left.rotation + shape_diff.arm_left.rotation * diff_factor,
                lower: shape_current.arm_left.lower + shape_diff.arm_left.lower * diff_factor,
            },
            arm_right: AnimationArm {
                upper: [
                    shape_current.arm_right.upper[0] + shape_diff.arm_right.upper[0] * diff_factor,
                    shape_current.arm_right.upper[1] + shape_diff.arm_right.upper[1] * diff_factor,
                    shape_current.arm_right.upper[2] + shape_diff.arm_right.upper[2] * diff_factor,
                ],
                rotation: shape_current.arm_right.rotation + shape_diff.arm_right.rotation * diff_factor,
                lower: shape_current.arm_right.lower + shape_diff.arm_right.lower * diff_factor,
            },
            hand_left: AnimationHand {
                palm: [
                    shape_current.hand_left.palm[0] + shape_diff.hand_left.palm[0] * diff_factor,
                    shape_current.hand_left.palm[1] + shape_diff.hand_left.palm[1] * diff_factor,
                    shape_current.hand_left.palm[2] + shape_diff.hand_left.palm[2] * diff_factor,
                ],
                rotation: shape_current.hand_left.rotation + shape_diff.hand_left.rotation * diff_factor,
                thumb: AnimationFinger {
                    base: [
                        shape_current.hand_left.thumb.base[0] + shape_diff.hand_left.thumb.base[0] * diff_factor,
                        shape_current.hand_left.thumb.base[1] + shape_diff.hand_left.thumb.base[1] * diff_factor,
                        shape_current.hand_left.thumb.base[2] + shape_diff.hand_left.thumb.base[2] * diff_factor,
                    ],
                    rotation: shape_current.hand_left.thumb.rotation + shape_diff.hand_left.thumb.rotation * diff_factor,
                    middle: shape_current.hand_left.thumb.middle + shape_diff.hand_left.thumb.middle * diff_factor,
                    tip: shape_current.hand_left.thumb.tip + shape_diff.hand_left.thumb.tip * diff_factor,
                },
                index: AnimationFinger {
                    base: [
                        shape_current.hand_left.index.base[0] + shape_diff.hand_left.index.base[0] * diff_factor,
                        shape_current.hand_left.index.base[1] + shape_diff.hand_left.index.base[1] * diff_factor,
                        shape_current.hand_left.index.base[2] + shape_diff.hand_left.index.base[2] * diff_factor,
                    ],
                    rotation: shape_current.hand_left.index.rotation + shape_diff.hand_left.index.rotation * diff_factor,
                    middle: shape_current.hand_left.index.middle + shape_diff.hand_left.index.middle * diff_factor,
                    tip: shape_current.hand_left.index.tip + shape_diff.hand_left.index.tip * diff_factor,
                },
                middle: AnimationFinger {
                    base: [
                        shape_current.hand_left.middle.base[0] + shape_diff.hand_left.middle.base[0] * diff_factor,
                        shape_current.hand_left.middle.base[1] + shape_diff.hand_left.middle.base[1] * diff_factor,
                        shape_current.hand_left.middle.base[2] + shape_diff.hand_left.middle.base[2] * diff_factor,
                    ],
                    rotation: shape_current.hand_left.middle.rotation + shape_diff.hand_left.middle.rotation * diff_factor,
                    middle: shape_current.hand_left.middle.middle + shape_diff.hand_left.middle.middle * diff_factor,
                    tip: shape_current.hand_left.middle.tip + shape_diff.hand_left.middle.tip * diff_factor,
                },
                ring: AnimationFinger {
                    base: [
                        shape_current.hand_left.ring.base[0] + shape_diff.hand_left.ring.base[0] * diff_factor,
                        shape_current.hand_left.ring.base[1] + shape_diff.hand_left.ring.base[1] * diff_factor,
                        shape_current.hand_left.ring.base[2] + shape_diff.hand_left.ring.base[2] * diff_factor,
                    ],
                    rotation: shape_current.hand_left.ring.rotation + shape_diff.hand_left.ring.rotation * diff_factor,
                    middle: shape_current.hand_left.ring.middle + shape_diff.hand_left.ring.middle * diff_factor,
                    tip: shape_current.hand_left.ring.tip + shape_diff.hand_left.ring.tip * diff_factor,
                },
                pinkie: AnimationFinger {
                    base: [
                        shape_current.hand_left.pinkie.base[0] + shape_diff.hand_left.pinkie.base[0] * diff_factor,
                        shape_current.hand_left.pinkie.base[1] + shape_diff.hand_left.pinkie.base[1] * diff_factor,
                        shape_current.hand_left.pinkie.base[2] + shape_diff.hand_left.pinkie.base[2] * diff_factor,
                    ],
                    rotation: shape_current.hand_left.pinkie.rotation + shape_diff.hand_left.pinkie.rotation * diff_factor,
                    middle: shape_current.hand_left.pinkie.middle + shape_diff.hand_left.pinkie.middle * diff_factor,
                    tip: shape_current.hand_left.pinkie.tip + shape_diff.hand_left.pinkie.tip * diff_factor,
                },
            },
            hand_right: AnimationHand {
                palm: [
                    shape_current.hand_right.palm[0] + shape_diff.hand_right.palm[0] * diff_factor,
                    shape_current.hand_right.palm[1] + shape_diff.hand_right.palm[1] * diff_factor,
                    shape_current.hand_right.palm[2] + shape_diff.hand_right.palm[2] * diff_factor,
                ],
                rotation: shape_current.hand_right.rotation + shape_diff.hand_right.rotation * diff_factor,
                thumb: AnimationFinger {
                    base: [
                        shape_current.hand_right.thumb.base[0] + shape_diff.hand_right.thumb.base[0] * diff_factor,
                        shape_current.hand_right.thumb.base[1] + shape_diff.hand_right.thumb.base[1] * diff_factor,
                        shape_current.hand_right.thumb.base[2] + shape_diff.hand_right.thumb.base[2] * diff_factor,
                    ],
                    rotation: shape_current.hand_right.thumb.rotation + shape_diff.hand_right.thumb.rotation * diff_factor,
                    middle: shape_current.hand_right.thumb.middle + shape_diff.hand_right.thumb.middle * diff_factor,
                    tip: shape_current.hand_right.thumb.tip + shape_diff.hand_right.thumb.tip * diff_factor,
                },
                index: AnimationFinger {
                    base: [
                        shape_current.hand_right.index.base[0] + shape_diff.hand_right.index.base[0] * diff_factor,
                        shape_current.hand_right.index.base[1] + shape_diff.hand_right.index.base[1] * diff_factor,
                        shape_current.hand_right.index.base[2] + shape_diff.hand_right.index.base[2] * diff_factor,
                    ],
                    rotation: shape_current.hand_right.index.rotation + shape_diff.hand_right.index.rotation * diff_factor,
                    middle: shape_current.hand_right.index.middle + shape_diff.hand_right.index.middle * diff_factor,
                    tip: shape_current.hand_right.index.tip + shape_diff.hand_right.index.tip * diff_factor,
                },
                middle: AnimationFinger {
                    base: [
                        shape_current.hand_right.middle.base[0] + shape_diff.hand_right.middle.base[0] * diff_factor,
                        shape_current.hand_right.middle.base[1] + shape_diff.hand_right.middle.base[1] * diff_factor,
                        shape_current.hand_right.middle.base[2] + shape_diff.hand_right.middle.base[2] * diff_factor,
                    ],
                    rotation: shape_current.hand_right.middle.rotation + shape_diff.hand_right.middle.rotation * diff_factor,
                    middle: shape_current.hand_right.middle.middle + shape_diff.hand_right.middle.middle * diff_factor,
                    tip: shape_current.hand_right.middle.tip + shape_diff.hand_right.middle.tip * diff_factor,
                },
                ring: AnimationFinger {
                    base: [
                        shape_current.hand_right.ring.base[0] + shape_diff.hand_right.ring.base[0] * diff_factor,
                        shape_current.hand_right.ring.base[1] + shape_diff.hand_right.ring.base[1] * diff_factor,
                        shape_current.hand_right.ring.base[2] + shape_diff.hand_right.ring.base[2] * diff_factor,
                    ],
                    rotation: shape_current.hand_right.ring.rotation + shape_diff.hand_right.ring.rotation * diff_factor,
                    middle: shape_current.hand_right.ring.middle + shape_diff.hand_right.ring.middle * diff_factor,
                    tip: shape_current.hand_right.ring.tip + shape_diff.hand_right.ring.tip * diff_factor,
                },
                pinkie: AnimationFinger {
                    base: [
                        shape_current.hand_right.pinkie.base[0] + shape_diff.hand_right.pinkie.base[0] * diff_factor,
                        shape_current.hand_right.pinkie.base[1] + shape_diff.hand_right.pinkie.base[1] * diff_factor,
                        shape_current.hand_right.pinkie.base[2] + shape_diff.hand_right.pinkie.base[2] * diff_factor,
                    ],
                    rotation: shape_current.hand_right.pinkie.rotation + shape_diff.hand_right.pinkie.rotation * diff_factor,
                    middle: shape_current.hand_right.pinkie.middle + shape_diff.hand_right.pinkie.middle * diff_factor,
                    tip: shape_current.hand_right.pinkie.tip + shape_diff.hand_right.pinkie.tip * diff_factor,
                },
            },
            ..Default::default()
        }
    }

    pub fn shape_diff(
        shape_current: AnimationShape,
        shape_next: AnimationShape,
    ) -> Self {
        Self {
            arm_left: AnimationArm {
                upper: [
                    shape_next.arm_left.upper[0] - shape_current.arm_left.upper[0],
                    shape_next.arm_left.upper[1] - shape_current.arm_left.upper[1],
                    shape_next.arm_left.upper[2] - shape_current.arm_left.upper[2],
                ],
                rotation: shape_next.arm_left.rotation - shape_current.arm_left.rotation,
                lower: shape_next.arm_left.lower - shape_current.arm_left.lower,
            },
            arm_right: AnimationArm {
                upper: [
                    shape_next.arm_right.upper[0] - shape_current.arm_right.upper[0],
                    shape_next.arm_right.upper[1] - shape_current.arm_right.upper[1],
                    shape_next.arm_right.upper[2] - shape_current.arm_right.upper[2],
                ],
                rotation: shape_next.arm_right.rotation - shape_current.arm_right.rotation,
                lower: shape_next.arm_right.lower - shape_current.arm_right.lower,
            },
            hand_left: AnimationHand {
                palm: [
                    shape_next.hand_left.palm[0] - shape_current.hand_left.palm[0],
                    shape_next.hand_left.palm[1] - shape_current.hand_left.palm[1],
                    shape_next.hand_left.palm[2] - shape_current.hand_left.palm[2],
                ],
                rotation: shape_next.hand_left.rotation - shape_current.hand_left.rotation,
                thumb: AnimationFinger {
                    base: [
                        shape_next.hand_left.thumb.base[0] - shape_current.hand_left.thumb.base[0],
                        shape_next.hand_left.thumb.base[1] - shape_current.hand_left.thumb.base[1],
                        shape_next.hand_left.thumb.base[2] - shape_current.hand_left.thumb.base[2],
                    ],
                    rotation: shape_next.hand_left.thumb.rotation - shape_current.hand_left.thumb.rotation,
                    middle: shape_next.hand_left.thumb.middle - shape_current.hand_left.thumb.middle,
                    tip: shape_next.hand_left.thumb.tip - shape_current.hand_left.thumb.tip,
                },
                index: AnimationFinger {
                    base: [
                        shape_next.hand_left.index.base[0] - shape_current.hand_left.index.base[0],
                        shape_next.hand_left.index.base[1] - shape_current.hand_left.index.base[1],
                        shape_next.hand_left.index.base[2] - shape_current.hand_left.index.base[2],
                    ],
                    rotation: shape_next.hand_left.index.rotation - shape_current.hand_left.index.rotation,
                    middle: shape_next.hand_left.index.middle - shape_current.hand_left.index.middle,
                    tip: shape_next.hand_left.index.tip - shape_current.hand_left.index.tip,
                },
                middle: AnimationFinger {
                    base: [
                        shape_next.hand_left.middle.base[0] - shape_current.hand_left.middle.base[0],
                        shape_next.hand_left.middle.base[1] - shape_current.hand_left.middle.base[1],
                        shape_next.hand_left.middle.base[2] - shape_current.hand_left.middle.base[2],
                    ],
                    rotation: shape_next.hand_left.middle.rotation - shape_current.hand_left.middle.rotation,
                    middle: shape_next.hand_left.middle.middle - shape_current.hand_left.middle.middle,
                    tip: shape_next.hand_left.middle.tip - shape_current.hand_left.middle.tip,
                },
                ring: AnimationFinger {
                    base: [
                        shape_next.hand_left.ring.base[0] - shape_current.hand_left.ring.base[0],
                        shape_next.hand_left.ring.base[1] - shape_current.hand_left.ring.base[1],
                        shape_next.hand_left.ring.base[2] - shape_current.hand_left.ring.base[2],
                    ],
                    rotation: shape_next.hand_left.ring.rotation - shape_current.hand_left.ring.rotation,
                    middle: shape_next.hand_left.ring.middle - shape_current.hand_left.ring.middle,
                    tip: shape_next.hand_left.ring.tip - shape_current.hand_left.ring.tip,
                },
                pinkie: AnimationFinger {
                    base: [
                        shape_next.hand_left.pinkie.base[0] - shape_current.hand_left.pinkie.base[0],
                        shape_next.hand_left.pinkie.base[1] - shape_current.hand_left.pinkie.base[1],
                        shape_next.hand_left.pinkie.base[2] - shape_current.hand_left.pinkie.base[2],
                    ],
                    rotation: shape_next.hand_left.pinkie.rotation - shape_current.hand_left.pinkie.rotation,
                    middle: shape_next.hand_left.pinkie.middle - shape_current.hand_left.pinkie.middle,
                    tip: shape_next.hand_left.pinkie.tip - shape_current.hand_left.pinkie.tip,
                },
            },
            hand_right: AnimationHand {
                palm: [
                    shape_next.hand_right.palm[0] - shape_current.hand_right.palm[0],
                    shape_next.hand_right.palm[1] - shape_current.hand_right.palm[1],
                    shape_next.hand_right.palm[2] - shape_current.hand_right.palm[2],
                ],
                rotation: shape_next.hand_right.rotation - shape_current.hand_right.rotation,
                thumb: AnimationFinger {
                    base: [
                        shape_next.hand_right.thumb.base[0] - shape_current.hand_right.thumb.base[0],
                        shape_next.hand_right.thumb.base[1] - shape_current.hand_right.thumb.base[1],
                        shape_next.hand_right.thumb.base[2] - shape_current.hand_right.thumb.base[2],
                    ],
                    rotation: shape_next.hand_right.thumb.rotation - shape_current.hand_right.thumb.rotation,
                    middle: shape_next.hand_right.thumb.middle - shape_current.hand_right.thumb.middle,
                    tip: shape_next.hand_right.thumb.tip - shape_current.hand_right.thumb.tip,
                },
                index: AnimationFinger {
                    base: [
                        shape_next.hand_right.index.base[0] - shape_current.hand_right.index.base[0],
                        shape_next.hand_right.index.base[1] - shape_current.hand_right.index.base[1],
                        shape_next.hand_right.index.base[2] - shape_current.hand_right.index.base[2],
                    ],
                    rotation: shape_next.hand_right.index.rotation - shape_current.hand_right.index.rotation,
                    middle: shape_next.hand_right.index.middle - shape_current.hand_right.index.middle,
                    tip: shape_next.hand_right.index.tip - shape_current.hand_right.index.tip,
                },
                middle: AnimationFinger {
                    base: [
                        shape_next.hand_right.middle.base[0] - shape_current.hand_right.middle.base[0],
                        shape_next.hand_right.middle.base[1] - shape_current.hand_right.middle.base[1],
                        shape_next.hand_right.middle.base[2] - shape_current.hand_right.middle.base[2],
                    ],
                    rotation: shape_next.hand_right.middle.rotation - shape_current.hand_right.middle.rotation,
                    middle: shape_next.hand_right.middle.middle - shape_current.hand_right.middle.middle,
                    tip: shape_next.hand_right.middle.tip - shape_current.hand_right.middle.tip,
                },
                ring: AnimationFinger {
                    base: [
                        shape_next.hand_right.ring.base[0] - shape_current.hand_right.ring.base[0],
                        shape_next.hand_right.ring.base[1] - shape_current.hand_right.ring.base[1],
                        shape_next.hand_right.ring.base[2] - shape_current.hand_right.ring.base[2],
                    ],
                    rotation: shape_next.hand_right.ring.rotation - shape_current.hand_right.ring.rotation,
                    middle: shape_next.hand_right.ring.middle - shape_current.hand_right.ring.middle,
                    tip: shape_next.hand_right.ring.tip - shape_current.hand_right.ring.tip,
                },
                pinkie: AnimationFinger {
                    base: [
                        shape_next.hand_right.pinkie.base[0] - shape_current.hand_right.pinkie.base[0],
                        shape_next.hand_right.pinkie.base[1] - shape_current.hand_right.pinkie.base[1],
                        shape_next.hand_right.pinkie.base[2] - shape_current.hand_right.pinkie.base[2],
                    ],
                    rotation: shape_next.hand_right.pinkie.rotation - shape_current.hand_right.pinkie.rotation,
                    middle: shape_next.hand_right.pinkie.middle - shape_current.hand_right.pinkie.middle,
                    tip: shape_next.hand_right.pinkie.tip - shape_current.hand_right.pinkie.tip,
                },
            },
            ..Default::default()
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
