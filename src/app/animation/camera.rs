
use crate::constants::{
    CAMERA_DRAG_SPEED,
    CAMERA_SCROLL_SPEED,
    DEFAULT_ANGLE_XZ,
    DEFAULT_ANGLE_Y,
    DEFAULT_DIST,
    DEFAULT_FOV,
    DEFAULT_MOVE_H,
    DEFAULT_MOVE_V,
    // DEFAULT_Z_FAR,
    DEFAULT_Z_NEAR,
    START_Z,
};
use eframe::{
    egui::{
        Rect,
        Sense,
        Ui,
    },
    emath::Vec2,
};
use glam::{
    Mat4,
    Vec3,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, Copy)]
pub struct Camera {
    pub angle_xz: f32,
    pub angle_y: f32,
    pub aspect_ratio: f32,
    pub dist: f32,
    pub mirrored: bool,
    pub move_h: f32,
    pub move_v: f32,
    // pub scale_factor: f32,
}

impl Default for Camera {
    fn default() -> Self {
        Self {
            angle_xz: DEFAULT_ANGLE_XZ,
            angle_y: DEFAULT_ANGLE_Y,
            aspect_ratio: 1.0, // ???
            dist: DEFAULT_DIST,
            mirrored: false,
            move_h: DEFAULT_MOVE_H,
            move_v: DEFAULT_MOVE_V,
            // scale_factor: DEFAULT_SCALE_FACTOR,
        }
    }
}

impl Camera {
    pub fn prepare_frame(
        &mut self,
        ui: &mut Ui,
    ) -> Rect {
        let size: Vec2 = ui
            .available_size_before_wrap();
        let (rect, response) = ui
            .allocate_exact_size(
                size,
                Sense::click_and_drag(),
            );
        self.angle_xz += CAMERA_DRAG_SPEED
            *
            response
                .drag_delta()
                .y;
        self.angle_y += CAMERA_DRAG_SPEED
            *
            response
                .drag_delta()
                .x;
        self.aspect_ratio = rect
            .aspect_ratio();
        ui
            .input(
                |input| {
                    self.dist -= CAMERA_SCROLL_SPEED
                        *
                        input
                            .smooth_scroll_delta
                            .y; // check, if dist > 0!!!
                    if
                        self
                            .dist
                        <
                        0.01
                    {
                        self.dist = 0.01;
                    }
                }
            );

        rect
    }

    pub fn to_uniform_data(
        self, // &self, ??? clippy
    ) -> [f32; 16 * 2 + 1] {
        const MODEL_CENTER: Vec3 = Vec3 {
            x: 0.0,
            y: 0.0,
            z: START_Z,
        };

        let proj: Mat4 = Mat4::perspective_infinite_rh(
        // let proj: Mat4 = Mat4::perspective_rh(
            DEFAULT_FOV, // fov_y_radians
            self
                .aspect_ratio,
            DEFAULT_Z_NEAR, // z_near
            // DEFAULT_Z_FAR, // z_far
        );
        let angle_xz: f32 = - std::f32::consts::PI * (self.angle_xz + 1.0);
        let angle_y: f32 = - std::f32::consts::PI * (self.angle_y + 1.0);

        let angle_xz_cos: f32 = angle_xz.cos();
        let eye: Vec3 = Vec3::new(
            MODEL_CENTER.x + self.dist * angle_xz_cos * angle_y.sin(),
            MODEL_CENTER.y + self.dist * angle_xz.sin(),
            MODEL_CENTER.z + self.dist * angle_xz_cos * angle_y.cos(),
        );
        let up: Vec3 = Vec3::Y;
        let view: Mat4 = Mat4::look_at_rh(
            eye, // cam_pos
            MODEL_CENTER,
            up,
        );
        // let proj_inv = proj.inverse();
        // let mut raw = [0f32; 16 * 3 + 4];
        // raw[..16].copy_from_slice(&AsRef::<[f32; 16]>::as_ref(&proj)[..]);
        // raw[16..32].copy_from_slice(&AsRef::<[f32; 16]>::as_ref(&proj_inv)[..]);
        // raw[32..48].copy_from_slice(&AsRef::<[f32; 16]>::as_ref(&view)[..]);
        // raw[48..51].copy_from_slice(AsRef::<[f32; 3]>::as_ref(&cam_pos));
        // raw[51] = 1.0;

        let mut raw = [0f32; 16 * 2 + 1];
        raw[..16]
            .copy_from_slice(
                &AsRef::<[f32; 16]>::as_ref(
                    &proj
                )[..]
            );
        raw[16..32]
            .copy_from_slice(
                &AsRef::<[f32; 16]>::as_ref(
                    &view
                )[..]
            );
        raw[32] = 1.0; // ??? what for ???
        raw
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
