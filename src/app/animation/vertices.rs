
use crate::{
    app::animation::skeleton::Limb,
    constants::VERTEX_W,
};
use bytemuck::{Pod, Zeroable};
use glam::Vec3;

// ----------------------------------------------------------------------------

#[repr(C)]
#[derive(Clone, Copy, Pod, Zeroable)]
pub struct Vertex {
    pub color: [f32; 4],
    pub pos: [f32; 4],
    // pub tex_coord: [f32; 2],
}

// ----------------------------------------------------------------------------

#[derive(Clone)]
// Intermediary format.
// Combines limbs and the palm rectangle.
pub struct Rectangle {
    pub color: [f32; 4],
    pub start: Vec3,
    pub end: Vec3,
    pub width: Vec3,
}

impl Rectangle {
    pub fn from_limb(
        limb: Limb,
    ) -> Self {
        Self {
            color: limb
                .color,
            start: limb
                .start,
            end: limb
                .end,
            width: limb
                .width,
        }
    }

    pub fn create_vertices(
        &self,
        mut vertices: Vec<Vertex>,
    ) -> Vec<Vertex> {
        // Add vertices:
        vertices
            .push(
                Vertex {
                    color: self.color,
                    pos: (
                        self.start + self.width
                    )
                        .extend(
                            VERTEX_W
                        )
                        .to_array(),
                }
            );
        vertices
            .push(
                Vertex {
                    color: self.color,
                    pos: (
                        self.start - self.width
                    )
                        .extend(
                            VERTEX_W
                        )
                        .to_array(),
                }
            );
        vertices
            .push(
                Vertex {
                    color: self.color,
                    pos: (
                        self.end + self.width
                    )
                        .extend(
                            VERTEX_W
                        )
                        .to_array(),
                }
            );
        vertices
            .push(
                Vertex {
                    color: self.color,
                    pos: (
                        self.end - self.width
                    )
                        .extend(
                            VERTEX_W
                        )
                        .to_array(),
                }
            );
        vertices
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
