
use eframe::egui::{
    Context,
    Key,
};

// ----------------------------------------------------------------------------

pub struct KeyboardEvent {}

impl KeyboardEvent {
    pub fn refresh(
        context: &Context,
    ) -> bool {
        context
            .input(
                |input| {
                    input
                        .key_pressed(
                            Key::R
                        )
                }
            )
    }

    pub fn list_down(
        context: &Context,
    ) -> bool {
        context
            .input(
                |input| {
                    input
                        .key_pressed(
                            Key::ArrowDown
                        )
                }
            )
    }

    pub fn list_up(
        context: &Context,
    ) -> bool {
        context
            .input(
                |input| {
                    input
                        .key_pressed(
                            Key::ArrowUp
                        )
                }
            )
    }

    pub fn pause(
        context: &Context,
    ) -> bool {
        context
            .input(
                |input| {
                    input
                        .key_pressed(
                            Key::Space
                        )
                }
            )
    }

    pub fn restart(
        context: &Context,
    ) -> bool {
        context
            .input(
                |input| {
                    input
                        .key_pressed(
                            Key::ArrowLeft
                        )
                }
            )
    }

}
