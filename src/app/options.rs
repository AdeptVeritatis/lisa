
use crate::{
    app::localization::{
        get_string,
        set_locale,
    },
    constants::DEFAULT_LANGUAGE,
    dictionary::{
        categories::Category,
        sign::Sign,
        sign_language::SignLanguage,
        Dictionary,
    },
};
use eframe::egui::{
    ThemePreference,
    Ui,
};
use fluent::{
    bundle::FluentBundle,
    FluentResource,
};
use intl_memoizer::IntlLangMemoizer;
use std::{
    cell::RefCell,
    rc::Rc,
};
use unic_langid::LanguageIdentifier;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct AppOptions {
    pub category: Category,
    pub language: LanguageIdentifier,
    pub sign_language: SignLanguage,
    pub theme: ThemePreference,
}

impl Default for AppOptions {
    fn default() -> Self {
        Self {
            category: Category::All(
                DEFAULT_LANGUAGE
                    .to_string()
            ),
            language: DEFAULT_LANGUAGE,
            sign_language: SignLanguage::LSF,
            theme: ThemePreference::System,
        }
    }
}

impl AppOptions {
    /// Set app language.
    pub fn set_language(
        &mut self,
        description: &str,
        current_sign: Rc<RefCell<Option<Sign>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        ftl_string: &str,
        identifier: LanguageIdentifier,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
        ui: &mut Ui,
    ) {
        if
            ui
                .button(
                    description,
                )
                .clicked()
        {
            set_locale(
                ftl_string,
                identifier
                    .clone(),
                localization
                    .clone(),
            );

            // Create new sign from key.
            // But key has changed, because it is translated now.
            // Create a new list with new keys.
            // Translate the old key into new language.
            // Get new Sign::from_key().
            self.language = identifier;

            // Translate categories:
            self.category = self
                .category
                .translate(
                    self
                        .language
                        .to_string()
                );

            options
                .replace(
                    self
                        .to_owned()
                );
            let dictionary_borrow = dictionary
                .borrow()
                .to_owned();
            dictionary_borrow
                .update(
                    current_sign,
                    dictionary,
                    localization,
                    options,
                );

            ui
                .close_menu();
        };
    }

    pub fn set_sign_language( // needs better name!!!
        &self,
        current_sign: Rc<RefCell<Option<Sign>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
    ) {
        // update app.current_frame too ???????????????????????????
        // does not update step durations ???
        let current_sign_borrow: Option<Sign> = current_sign
            .borrow()
            .to_owned();
        match
            current_sign_borrow
        {
            None => {
                println!("!! no update for current sign");
            },
            Some(sign) => {
                let version: String = match
                    sign
                        .version
                {
                    None => String::new(),
                    Some(version) => version,
                };
                let key_new: String = match
                    self
                        .language
                        .to_string()
                        .as_str()
                {
                    "en-US" => {
                        let mut key: String = sign
                            .english
                            .clone();
                        key
                            .push_str(
                                version
                                    .as_str()
                            );
                        key
                    },
                    "fr-FR" => {
                        let mut key: String = sign
                        .french
                        .clone();
                        key
                            .push_str(
                                version
                                    .as_str()
                            );
                        key
                    },
                    "de-DE" => {
                        let mut key: String = sign
                        .german
                        .clone();
                        key
                            .push_str(
                                version
                                    .as_str()
                            );
                        key
                    },
                    _ => {
                        let mut key: String = sign
                        .english
                        .clone();
                        key
                            .push_str(
                                version
                                    .as_str()
                            );
                        key
                    },
                };

                current_sign
                    .replace(
                        Some(
                            Sign::from_key(
                                self
                                    .to_owned(),
                                dictionary,
                                key_new,
                                localization,
                            )
                        )
                    );
            },
        };
    }

    /// Set dark / light theme mode.
    pub fn set_theme(
        &mut self,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
        ui: &mut Ui,
    ) {
        let light: String = get_string(
            Rc::clone(&localization),
            "light",
        );
        let dark: String = get_string(
            Rc::clone(&localization),
            "dark",
        );
        let system: String = get_string(
            Rc::clone(&localization),
            "system",
        );

        ui
            .vertical(
                |ui_theme| {
                    ui_theme
                        .set_min_width(
                            72.0,
                        );
                    ui_theme
                        .selectable_value(
                            &mut self
                                .theme,
                            ThemePreference::Light,
                            format!("☀ {light}"),
                        );
                    ui_theme
                        .separator();
                    ui_theme
                        .selectable_value(
                            &mut self
                                .theme,
                            ThemePreference::Dark,
                            format!("🌙 {dark}"),
                        );
                    ui_theme
                        .separator();
                    ui_theme
                        .selectable_value(
                            &mut self
                                .theme,
                            ThemePreference::System,
                            format!("💻 {system}"),
                        );
                },
            );
        options
            .replace(
                    self
                        .to_owned()
            );
    }

}
