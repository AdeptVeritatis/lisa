#![allow(clippy::single_match)]

use crate::constants::LOCALE_EN_US;
use fluent::{
    bundle::FluentBundle,
    FluentResource,
};
use intl_memoizer::IntlLangMemoizer;
use std::{
    cell::RefCell,
    rc::Rc,
};
use unic_langid::{
    langid,
    LanguageIdentifier,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn default_locale(
) -> FluentBundle<FluentResource, IntlLangMemoizer> {
    let mut bundle: FluentBundle<FluentResource, IntlLangMemoizer> =
    FluentBundle::new(
        vec![
            langid!("en-US"),
            // langid!("de-DE"),
            // langid!("fr-FR"),
        ],
    );

    let ftl_string: String = String::from(LOCALE_EN_US);
    match
        FluentResource::try_new(ftl_string)
    {
        Err((_resource, error)) => println!("!! error: {error:?}"),
        Ok(resource) => match
            bundle
                .add_resource(resource)
        {
            Err(error) => println!("!! error: {error:?}"),
            Ok(_) => (),
        },
    };
    bundle
}

pub fn get_string(
    localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
    request: &str,
) -> String {
    let bundle = localization
        .borrow();
    match
        bundle
            .get_message(request)
    {
        None => String::from(request),
        Some(message) => match
            message
                .value()
        {
            None => String::from(request),
            Some(pattern) => {
                bundle
                    .format_pattern(
                        pattern,
                        None,
                        &mut Vec::new(), // error!!!
                    )
                    .to_string()
            },
        },
    }
}

pub fn set_locale(
    ftl_string: &str,
    identifier: LanguageIdentifier,
    localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
) {
    let mut bundle: FluentBundle<FluentResource, IntlLangMemoizer> =
    FluentBundle::new(
        vec![
            identifier,
        ],
    );
    match
        FluentResource::try_new(
            String::from(ftl_string)
        )
    {
        Err((_resource, error)) => println!("!! error: {error:?}"),
        Ok(resource) => match
            bundle
                .add_resource(resource)
        {
            Err(error) => println!("!! error: {error:?}"),
            Ok(_) => (),
        },
    };
    localization
        .replace(bundle);
}
