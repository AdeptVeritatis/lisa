
use crate::{
    app::{
        animation::{
            camera::Camera,
            CurrentStep,
        },
        ui::elements::ui_frame::UiElementFrame,
    },
    dictionary::sign::Sign,
};
use eframe::egui::{
    CentralPanel,
    Context,
};
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct UiCentralPanel {}

impl UiCentralPanel {
    pub fn draw(
        camera: Rc<RefCell<Camera>>,
        context: &Context,
        current_sign: Rc<RefCell<Option<Sign>>>,
        current_step: Rc<RefCell<Option<CurrentStep>>>,
    ) {
    // Use shape to draw:
        CentralPanel::default()
            .show(
                context,
                |ui_central| {
                    // ui_central
                    //     .heading(selected);
                // Body model:
                    ui_central
                        .vertical_centered(
                            |ui_centered| {
                                eframe::egui::Frame::canvas(
                                    ui_centered
                                        .style()
                                )
                                .show(
                                    ui_centered,
                                    |ui_model| {
                                        UiElementFrame::draw(
                                            camera,
                                            context,
                                            current_sign,
                                            current_step,
                                            ui_model,
                                        );
                                    },
                                );
                            },
                        );
                },
            );
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
