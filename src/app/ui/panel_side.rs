// #![allow(clippy::manual_map)]

use crate::{
    app::{
        animation::camera::Camera,
        localization::get_string,
        options::AppOptions,
        ui::elements::{
            ui_category::UiElementCategory,
            ui_dictionary::UiElementDictionary,
            ui_view::UiElementView,
        },
    },
    constants::{
        DEFAULT_SIDE_PANEL_SPACE,
        DEFAULT_SIDE_PANEL_WIDTH,
        SIDE_PANEL_SPACE_NEXT,
    },
    dictionary::{
        sign::Sign,
        Dictionary,
    },
};
use eframe::egui::{
    containers::panel::{
        Side,
        SidePanel,
    },
    Context,
    Id,
    ScrollArea,
    Ui,
};
use fluent::{
    bundle::FluentBundle,
    FluentResource,
};
use intl_memoizer::IntlLangMemoizer;
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct UiSidePanel {}

impl UiSidePanel {
    pub fn draw(
        camera: Rc<RefCell<Camera>>,
        context: &Context,
        current_sign: Rc<RefCell<Option<Sign>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
    ) {
        SidePanel::new(
            Side::Left,
            Id::new("side"),
        )
        // .exact_width(DEFAULT_SIDE_PANEL_WIDTH)
        .default_width(DEFAULT_SIDE_PANEL_WIDTH)
        .show(
            context,
            move |ui_panel| {
                ScrollArea::vertical()
                    .show(
                        ui_panel,
                        move |ui_scroll| {
                            ui_scroll
                                .vertical_centered_justified(
                                    |ui_inner| {
                                        // ui_inner
                                        //     .set_max_width(240.0);
                                        ui_inner
                                            .add_space(DEFAULT_SIDE_PANEL_SPACE);
                                    // Sign language:
                                        UiSidePanel::create_header(
                                            "sign_language",
                                            localization
                                                .clone(),
                                            ui_inner,
                                        );
                                        UiElementDictionary::select(
                                            current_sign
                                                .clone(),
                                            dictionary
                                                .clone(),
                                            localization
                                                .clone(),
                                            options
                                                .clone(),
                                            ui_inner,
                                        );
                                        ui_inner
                                            .add_space(SIDE_PANEL_SPACE_NEXT);
                                    // Category:
                                        UiSidePanel::create_header(
                                            "category",
                                            localization
                                                .clone(),
                                            ui_inner,
                                        );
                                        UiElementCategory::select(
                                            current_sign
                                                .clone(),
                                            dictionary
                                                .clone(),
                                            localization
                                                .clone(),
                                            options
                                                .clone(),
                                            ui_inner,
                                        );
                                        ui_inner
                                            .add_space(SIDE_PANEL_SPACE_NEXT);
                                    // View:
                                        UiSidePanel::create_header(
                                            "view", // "camera",
                                            localization
                                                .clone(),
                                            ui_inner,
                                        );
                                        UiElementView::set(
                                            camera
                                                .clone(),
                                            ui_inner,
                                        );
                                        ui_inner
                                            .add_space(DEFAULT_SIDE_PANEL_SPACE);
                                        UiElementView::reset(
                                            camera
                                                .clone(),
                                            localization
                                                .clone(),
                                            ui_inner,
                                        );
                                        ui_inner
                                            .add_space(DEFAULT_SIDE_PANEL_SPACE);
                                        UiElementView::mirror(
                                            camera
                                                .clone(),
                                            localization
                                                .clone(),
                                            ui_inner,
                                        );
                                        ui_inner
                                            .add_space(SIDE_PANEL_SPACE_NEXT);
                                    // Dictionary editing:
                                        UiSidePanel::create_header_editing(
                                            localization
                                                .clone(),
                                            ui_inner,
                                        );
                                        UiElementDictionary::load(
                                            current_sign
                                                .clone(),
                                            dictionary
                                                .clone(),
                                            localization
                                                .clone(),
                                            options
                                                .clone(),
                                            ui_inner,
                                        );
                                        ui_inner
                                            .add_space(DEFAULT_SIDE_PANEL_SPACE);
                                        UiElementDictionary::reload(
                                            context,
                                            current_sign,
                                            dictionary,
                                            localization,
                                            options,
                                            ui_inner,
                                        );
                                    }
                                );
                        },
                    );
            },
        );
    }

}

impl UiSidePanel {
    fn create_header(
        key: &str,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        ui: &mut Ui,
    ) {
        ui
            .separator();
        ui
            .vertical_centered(
                |ui_label| {
                    ui_label
                        .label(
                            format!(
                                "{}:",
                                get_string(
                                    localization,
                                    key,
                                ),
                            )
                        );
                }
            );
        ui
            .separator();
        ui
            .add_space(DEFAULT_SIDE_PANEL_SPACE);
    }

    fn create_header_editing(
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        ui: &mut Ui,
    ) {
        ui
            .separator();
        ui
            .small(
                get_string(
                    localization
                        .clone(),
                    "for_easy_dictionary_editing_1",
                )
            );
        ui
            .label(
                get_string(
                    localization
                        .clone(),
                    "for_easy_dictionary_editing_2",
                )
            );
        ui
            .label(
                format!(
                    "{}:",
                    get_string(
                        localization
                            .clone(),
                        "for_easy_dictionary_editing_3",
                    ),
                )
            );
        ui
            .separator();
        ui
            .add_space(DEFAULT_SIDE_PANEL_SPACE);
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
