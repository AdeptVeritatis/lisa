
use crate::{
    app::{
        localization::get_string,
        AppOptions,
    },
    constants::{
        LOCALE_DE_DE,
        LOCALE_EN_US,
        LOCALE_FR_FR,
    },
    dictionary::{
        sign::Sign,
        Dictionary,
    },
};
use eframe::egui::{
    Context,
    TopBottomPanel,
    Ui,
};
use fluent::{
    bundle::FluentBundle,
    FluentResource,
};
use intl_memoizer::IntlLangMemoizer;
use std::{
    cell::RefCell,
    rc::Rc,
};
use unic_langid::langid;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct UiMenu {}

impl UiMenu {
    pub fn draw(
        context: &Context,
        current_sign: Rc<RefCell<Option<Sign>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
    ) {
        TopBottomPanel::top("Menu")
            .show(
                context,
                move |ui| {
                    ui.horizontal(
                        |ui_menu| {
                        // Language:
                            UiMenu::draw_language(
                                current_sign,
                                dictionary,
                                localization
                                    .clone(),
                                options
                                    .clone(),
                                ui_menu,
                            );
                        // Theme:
                            UiMenu::draw_theme(
                                localization,
                                options,
                                ui_menu,
                            );
                        },
                    );
                },
            );
    }
}

impl UiMenu {
    fn draw_language(
        current_sign: Rc<RefCell<Option<Sign>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
        ui: &mut Ui,
    ) {
        let mut app_options: AppOptions = options
            .borrow()
            .to_owned();

        let string_language = get_string(
            Rc::clone(&localization),
            "language",
        );

        ui
            .menu_button(
                string_language,
                |ui_language| {
                    // English:
                    app_options
                        .set_language(
                            "English", // "English 🇬🇧 🇺🇸",
                            current_sign
                                .clone(),
                            dictionary
                                .clone(),
                            LOCALE_EN_US,
                            langid!("en-US"),
                            localization
                                .clone(),
                            options
                                .clone(),
                            ui_language,
                        );
                    ui_language
                        .separator();
                    // French:
                    app_options
                        .set_language(
                            "Française", // "French 🇫🇷",
                            current_sign
                                .clone(),
                            dictionary
                                .clone(),
                            LOCALE_FR_FR,
                            langid!("fr-FR"),
                            localization
                                .clone(),
                            options
                                .clone(),
                            ui_language,
                        );
                    ui_language
                        .separator();
                    // German:
                    app_options
                        .set_language(
                            "Deutsch", // "German 🇩🇪",
                            current_sign,
                            dictionary,
                            LOCALE_DE_DE,
                            langid!("de-DE"),
                            localization,
                            options,
                            ui_language,
                        );
                },
            );
    }

    fn draw_theme(
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
        ui: &mut Ui,
    ) {
        let mut app_options: AppOptions = options
            .borrow()
            .to_owned();

        let string_theme = get_string(
            Rc::clone(&localization),
            "theme",
        );

        ui
            .menu_button(
                string_theme,
                |ui_theme| {
                    app_options
                        .set_theme(
                            localization,
                            options,
                            ui_theme,
                        );
                },
            );
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
