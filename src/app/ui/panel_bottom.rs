
use crate::{
    // app::options::AppOptions,
    constants::DEFAULT_BOTTOM_PANEL_SPACE,
};
use eframe::egui::{
    containers::panel::{
        TopBottomPanel,
        TopBottomSide,
    },
    // Color32,
    Context,
    Id,
    // RichText,
};
// use std::{
    // cell::RefCell,
    // rc::Rc,
    // sync::Arc,
// };

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct UiBottomPanel {}

impl UiBottomPanel {
    pub fn draw(
        context: &Context,
        cpu_usage: Option<f32>,
        // message: Arc<String>,
        // options: Rc<RefCell<AppOptions>>,
    ) {
        TopBottomPanel::new(
            TopBottomSide::Bottom,
            Id::new("bottom"),
        )
        // .exact_height(DEFAULT_BOTTOM_PANEL_HEIGHT)
        .show(
            context,
            move |ui_panel| {
                ui_panel
                    .horizontal(
                        |ui_inner| {
                            // let mut app_options: AppOptions = options
                            //     .borrow()
                            //     .to_owned();

                            ui_inner
                                .add_space(DEFAULT_BOTTOM_PANEL_SPACE);
                            ui_inner
                                .small("< 0 /");
                            ui_inner
                                .add_space(DEFAULT_BOTTOM_PANEL_SPACE);
                            ui_inner
                                .separator();
                            ui_inner
                                .add_space(DEFAULT_BOTTOM_PANEL_SPACE);

                            ui_inner
                                .small(
                                    format!(
                                        "CPU frame time {{ms}}: {:.1}",
                                        match
                                            cpu_usage
                                        {
                                            None => 0.0,
                                            Some(usage) => usage * 1000.0,
                                        },
                                    )
                                );
                            // ui_inner
                            //     .small(
                            //         RichText::new(
                            //             message
                            //                 .to_string()
                            //         )
                            //         .color(
                            //             Color32::DARK_RED
                            //         )
                            //     );

                            // options
                            //     .replace(
                            //             app_options
                            //     );
                        },
                    );
                // ui_panel
                //     .with_layout(
                //         Layout::right_to_left(
                //             Align::Min
                //         ),
                //         |ui_align| {
                //             ui_align
                //                 .small("< 0 /");
                //         },
                //      );
            },
        );
    }

}

