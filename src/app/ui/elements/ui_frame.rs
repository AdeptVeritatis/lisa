
use crate::{
    app::animation::{
        camera::Camera,
        render::BodyModelCallback,
        shape::AnimationShape,
        CurrentStep,
    },
    dictionary::sign::Sign,
};
use eframe::{
    egui::{
        Align,
        Context,
        Layout,
        Rect,
        Ui,
        UiBuilder,
    },
    egui_wgpu::Callback,
};
use std::{
    cell::RefCell,
    rc::Rc,
    time::Duration,
};

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct UiElementFrame {
    animation_shape: AnimationShape,
    note: String,
}

impl UiElementFrame {
    fn new(
        context: &Context,
        current_sign: Rc<RefCell<Option<Sign>>>,
        current_step: Rc<RefCell<Option<CurrentStep>>>,
    ) -> Self {
        match
            current_sign
                .borrow()
                .to_owned()
        {
            // No sign from dictionary selected.
            // Return greeter sign.
            None => Self {
                animation_shape: AnimationShape::example(),
                note: String::new(), // always new string???
            },
            // Sign from dictionary found.
            // Check if sign is animation or just single shape.
            Some(sign) => {
                let note: String = sign
                    .note
                    .clone();
                Self {
                    // Get shape for this step:
                    animation_shape: get_animation_step( // move to impl ???
                        context,
                        current_step,
                        sign,
                    ),
                    note,
                }
            },
        }
    }
}

impl UiElementFrame {
    pub fn draw(
        camera: Rc<RefCell<Camera>>,
        context: &Context,
        current_sign: Rc<RefCell<Option<Sign>>>,
        current_step: Rc<RefCell<Option<CurrentStep>>>,
        ui: &mut Ui,
    ) {
        let frame_element: UiElementFrame = UiElementFrame::new(
            context,
            current_sign,
            current_step,
        );

        // move to Camera::frame() completely
        let mut camera_mut: Camera = camera
            .borrow()
            .to_owned();
        let rect: Rect = camera_mut
            .prepare_frame(
                ui,
            );
        let rect_note: Rect = rect
            .with_min_y(
                rect
                    .min
                    .y
                +
                24.0,
            );
        ui
            .painter()
            .add(
                Callback::new_paint_callback(
                    rect,
                    BodyModelCallback {
                        animation_shape: frame_element
                            .animation_shape,
                        camera: camera_mut,
                        note: frame_element
                            .note
                            .clone(),
                    },
                )
            );
        if
            !frame_element
                .note
                .is_empty()
        {
            ui
                .new_child(
                    UiBuilder::new()
                        .max_rect(
                            rect_note,
                        )
                        .layout(
                            Layout::top_down(
                                Align::Center,
                            ),
                        ),
                )
                .heading(
                    frame_element
                        .note,
                );

        };

        camera
            .replace(
                camera_mut,
            );
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

fn get_animation_step(
    context: &Context,
    current_step: Rc<RefCell<Option<CurrentStep>>>,
    sign: Sign,
) -> AnimationShape {
    let current_step_borrow: Option<CurrentStep> = current_step
        .borrow()
        .to_owned();
    match
        current_step_borrow
    {
        // No animation, load first shape.
        None => match
            sign
                .animation
                .first()
        {
            None => AnimationShape::default(),
            Some(shape) => shape.to_owned(),
        },
        // Has animation, find current shape and diff (calculate if subframe = 0), calculate, (break) and request redraw.
        Some(current) => {
            create_animation(
                context,
                current,
                current_step,
                sign,
            )
        },
    }
}

fn create_animation(
    context: &Context,
    mut current: CurrentStep,
    current_step: Rc<RefCell<Option<CurrentStep>>>,
    sign: Sign,
) -> AnimationShape {
    // Create map_step_to_shape in sign.rs.
    // Load new shapes.
    // First shape has no current.step_duration set.
    // Use current.step_subframe instead to update all shapes.
    // Update current / diff shape & duration and set time_start and add current & next frame until frames.

    // New step with subframe = 0 started:
    if
        current
            .step_subframe
        ==
        0
    {
        // Only update time, if not last frame repeated again.
        current
            .update_time();
        // Update after time was updated to update time for last frame, too.
        current.step_current = current
            .step_next;
        current
            .select_next_step();

        if
            current
                .step_next
            >=
            current
                .steps
        {
            println!(
                "!! error: animation step greater than expected\n   steps:   {}\n   current: {}\n   next:    {}",
                current
                    .steps,
                current
                    .step_current,
                current
                    .step_next
            );
        };

        // Calculate diff shape from current and next shape.
        current
            .get_shapes(
                sign,
            );
    }


    let diff_time: Duration = match
        current
            .pause
    {
        true => {
            current
                .duration_diff
        },
        false => {
            current
                .time_start
                .elapsed()
        },
    };

    current.step_subframe += 1;

    // if time for next frame, set subframe to 0.
    if
        diff_time
        >=
        current
            .step_duration
    {
        current.step_subframe = 0;
    };

    current_step
        .replace(
            Some(
                current
                    .clone()
            )
        );

    // Request repaint until animation ended.
    if
        {
            current
                .step_current
            +
            1
            <
            current
                .steps
        }
        ||
        {
            diff_time
            <
            current
                .step_duration
        }
    {
        context
            .request_repaint();
    //     println!("request repaint");
    // } else {
    //     println!("no repaint");
    };

    match
        current
            .pos_current
        ==
        current
            .pos_next
    {
        true => {
            // Current == next, just show current without calculating.
            // println!("still frame");
            current
                .shape_start
        },
        false => {
            // Calculate shape from start, diff and time.
            let diff_factor: f32 = match
                diff_time
                    .as_secs_f32()
                /
                current
                    .step_duration
                    .as_secs_f32()
                // 0.5
            {
                diff_factor if diff_factor > 1.0 => 1.0,
                diff_factor => diff_factor,
            };
            let shape_current: AnimationShape = current
                .shape_start;
            let shape_diff: AnimationShape = current
                .shape_diff;


            AnimationShape::current_step(
                diff_factor,
                shape_current,
                shape_diff,
            )
        },
    }
}
