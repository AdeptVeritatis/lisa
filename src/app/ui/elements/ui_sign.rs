
use crate::{
    app::{
        animation::{
            shape::AnimationShape,
            CurrentStep,
            ShapeValue,
        },
        events::keyboard::KeyboardEvent,
        localization::get_string,
        options::AppOptions,
    },
    constants::MIN_TOP_PANEL_COLUMN_WIDTH,
    dictionary::{
        sign::Sign,
        Dictionary,
    },
};
use eframe::egui::{
    ComboBox,
    Context,
    Grid,
    TextEdit,
    Ui,
};
use fluent::{
    bundle::FluentBundle,
    FluentResource,
};
use intl_memoizer::IntlLangMemoizer;
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
};

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct UiElementSign {}

impl UiElementSign {
    pub fn filter(
        filter: Rc<RefCell<String>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        ui: &mut Ui,
        width: f32,
    ) {
        let mut filter_mut: String = filter
            .borrow()
            .to_owned();
        ui
            .horizontal(
                |ui_filter| {
                    ui_filter
                        .add(
                            TextEdit::singleline(
                                &mut filter_mut
                            )
                            .hint_text(
                                get_string(
                                    Rc::clone(&localization),
                                    "filter",
                                )
                            )
                            .desired_width(
                                width,
                            ),
                        );
                    if
                        ui_filter
                            .button(
                                "❌"
                            )
                            .clicked()
                    {
                        filter_mut = String::new();
                    };
                }
            );
        filter
            .replace(
                filter_mut
            );
    }

    pub fn navigate(
        context: &Context,
        current_sign: Rc<RefCell<Option<Sign>>>,
        current_step: Rc<RefCell<Option<CurrentStep>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
        ui: &mut Ui,
    ) {
        Grid::new("List")
            .show(
                ui,
                |ui_list| {
                    if
                        ui_list
                            .button(
                                "  ⬇  " // "⬇️"
                            )
                            .clicked()
                        |
                        // Or press key 'down':
                        KeyboardEvent::list_down(
                            context,
                        )
                    {
                        // println!("ii next sign selected");
                        let number: usize = match
                            current_sign
                                .borrow()
                                .to_owned()
                        {
                            None => 0,
                            Some(sign) => {
                                match
                                    sign
                                        .number
                                {
                                    number if
                                        number + 1
                                        >=
                                        dictionary
                                            .borrow()
                                            .list
                                            .len()
                                    => 0,
                                    number => number + 1,
                                }
                            },
                        };
                        // println!("{number}");
                        get_key_from_number(
                            current_sign
                                .clone(),
                            current_step
                                .clone(),
                            dictionary
                                .clone(),
                            localization
                                .clone(),
                            number,
                            options
                                .clone(),
                        );
                    };
                    if
                        ui_list
                            .button(
                                "  ⬆  " // "⬆️"
                            )
                            .clicked()
                        |
                        // Or press key 'up':
                        KeyboardEvent::list_up(
                            context,
                        )
                    {
                        // println!("ii previous sign selected");
                        let number: usize = match
                            current_sign
                                .borrow()
                                .to_owned()
                        {
                            None => 0, // 1 ???
                            Some(sign) => {
                                match
                                    sign
                                        .number
                                {
                                    0 => {
                                        dictionary
                                            .borrow()
                                            .list
                                            .len()
                                        -
                                        1
                                    },
                                    number => number - 1,
                                }
                            },
                        };
                        // println!("{number}");
                        get_key_from_number(
                            current_sign,
                            current_step,
                            dictionary,
                            localization,
                            number,
                            options,
                        );
                    };
                },
            );
    }

    pub fn select(
        current_sign: Rc<RefCell<Option<Sign>>>,
        current_step: Rc<RefCell<Option<CurrentStep>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        filter: Rc<RefCell<String>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
        panel_width: f32,
        ui: &mut Ui,
    ) {
        // better a list of buttons???
        // or move it to a another panel???
        let combobox_label = get_combobox_label(
            current_sign
                .clone(),
            localization
                .clone(),
            options
                .clone(),
        );
        ui
            .vertical_centered_justified(
                |ui_centered| {
                    ComboBox::from_id_salt("ComboBox Select")
                        .selected_text(combobox_label)
                        .width(
                            panel_width
                        )
                        .wrap()
                        .show_ui(
                            ui_centered,
                            |ui_combo| {
                                let mut check: bool = false;
                                let dictionary_value: Dictionary = dictionary
                                    .borrow()
                                    .to_owned();
                                let list: Vec<String> = dictionary_value
                                    .list;
                                for
                                    key
                                in
                                    list
                                        .iter()
                                        .filter(
                                            |sign_name| {
                                                sign_name
                                                    .to_ascii_lowercase()
                                                    .contains(
                                                        filter
                                                            .borrow()
                                                            .to_ascii_lowercase()
                                                            .as_str()
                                                    )
                                            }
                                        )
                                {
                                    ui_combo
                                        .selectable_value(
                                            &mut check,
                                            true,
                                            key,
                                        );
                                    if
                                        check
                                    {
                                        current_sign
                                            .replace(
                                                Some(
                                                    Sign::from_key(
                                                        options
                                                            .borrow()
                                                            .to_owned(),
                                                        dictionary
                                                            .clone(),
                                                        key
                                                            .to_owned(),
                                                        localization
                                                            .clone(),
                                                    )
                                                )
                                            );
                                        // println!("ii new sign selected");
                                        new_sign_selected(
                                            current_sign
                                                .clone(),
                                            current_step
                                                .clone(),
                                        );
                                        check = false;
                                    };
                                };
                            },
                        );
                },
            );
    }

    pub fn translation(
        current_sign: Rc<RefCell<Option<Sign>>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
        ui: &mut Ui,
    ) {
        let sign = match
            current_sign
                .borrow()
                .to_owned()
        {
            None => Sign::default(),
            Some(sign) => sign,
        };
        let description: String = format!(
            "{}:",
            get_string(
                Rc::clone(&localization),
                "description", //info
            ),
        );
        let english: String = format!(
            "{}:",
            get_string(
                Rc::clone(&localization),
                "english",
            ),
        );
        let french: String = format!(
            "{}:",
            get_string(
                Rc::clone(&localization),
                "french",
            ),
        );
        let german: String = format!(
            "{}:",
            get_string(
                Rc::clone(&localization),
                "german",
            ),
        );

        Grid::new("Translation")
            // .max_col_width(MAX_TOP_PANEL_COLUMN_WIDTH) // not a good solution, does not change!!!
            .min_col_width(MIN_TOP_PANEL_COLUMN_WIDTH)
            // .striped(true)
            .show(
                ui,
                |ui_grid| {
                    match
                        options
                            .borrow()
                            .language
                            .to_string()
                            .as_str()
                    {
                        "en-US" => {
                            // English!, F_rench, G_erman
                            ui_grid
                                .label(description);
                            ui_grid
                                .label(sign.description);
                            ui_grid
                                .end_row();
                            ui_grid
                                .label(english);
                            ui_grid
                                .label(sign.english);
                            ui_grid
                                .end_row();
                            ui_grid
                                .label(french);
                            ui_grid
                                .label(sign.french);
                            ui_grid
                                .end_row();
                            ui_grid
                                .label(german);
                            ui_grid
                                .label(sign.german);
                        },
                        "fr-FR" => {
                            // Française!, Al_lemande, An_glaise
                            ui_grid
                                .label(description);
                            ui_grid
                                .label(sign.description);
                            ui_grid
                                .end_row();
                            ui_grid
                                .label(french);
                            ui_grid
                                .label(sign.french);
                            ui_grid
                                .end_row();
                            ui_grid
                                .label(german);
                            ui_grid
                                .label(sign.german);
                            ui_grid
                                .end_row();
                            ui_grid
                                .label(english);
                            ui_grid
                                .label(sign.english);

                        },
                        "de-DE" => {
                            // Deutsch!, E_nglisch, F_ranzösisch
                            ui_grid
                                .label(description);
                            ui_grid
                                .label(sign.description);
                            ui_grid
                                .end_row();
                            ui_grid
                                .label(german);
                            ui_grid
                                .label(sign.german);
                            ui_grid
                                .end_row();
                            ui_grid
                                .label(english);
                            ui_grid
                                .label(sign.english);
                            ui_grid
                                .end_row();
                            ui_grid
                                .label(french);
                            ui_grid
                                .label(sign.french);

                        },
                        _ => {
                            ui_grid
                                .label(description);
                            ui_grid
                                .label(sign.description);
                            ui_grid
                                .end_row();
                            ui_grid
                                .label(english);
                            ui_grid
                                // .add(
                                //     Label::new(
                                //         sign
                                //             .english
                                //     )
                                //     .wrap(
                                //         true
                                //     )
                                .label(sign.english);
                            ui_grid
                                .end_row();
                            ui_grid
                                .label(french);
                            ui_grid
                                .label(sign.french);
                            ui_grid
                                .end_row();
                            ui_grid
                                .label(german);
                            ui_grid
                                .label(sign.german);
                        },
                    };
                },
            );
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

fn get_combobox_label(
    current_sign: Rc<RefCell<Option<Sign>>>,
    localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
    options: Rc<RefCell<AppOptions>>,
) -> String {
    match
        current_sign
            .borrow()
            .to_owned()
    {
        None => get_string(
            Rc::clone(&localization),
            "select_a_sign",
        ),
        Some(sign) => {
            let mut name: String = match
                options
                    .borrow()
                    .language
                    .to_string()
                    .as_str()
            {
                "en-US" => sign
                    .english,
                "fr-FR" => sign
                    .french,
                "de-DE" => sign
                    .german,
                _ => sign
                    .english,
            };
            match
                sign
                    .version
            {
                None => name,
                Some(version) => {
                    name
                        .push_str(
                            version
                                .as_str()
                        );
                    name
                },
            }
        },
    }
}

fn get_key_from_number( // use Sign::from_number() instead???
    current_sign: Rc<RefCell<Option<Sign>>>,
    current_step: Rc<RefCell<Option<CurrentStep>>>,
    dictionary: Rc<RefCell<Dictionary>>,
    localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
    number: usize,
    options: Rc<RefCell<AppOptions>>,
) {
    // get key from list with number
    match
        dictionary
            .borrow()
            .list
            .get(
                number
            )
    {
        None => {
            println!("!! error: index of list out of range: {number}");
        },
        Some(key) => {
            current_sign
                .replace(
                    Some(
                        Sign::from_key(
                            options
                                .borrow()
                                .to_owned(),
                            dictionary
                                .clone(),
                            key
                                .to_owned(),
                            localization,
                        )
                    )
                );
            new_sign_selected(
                current_sign
                    .clone(),
                current_step
                    .clone(),
            );
        },
    };
}

fn new_sign_selected(
    current_sign: Rc<RefCell<Option<Sign>>>,
    current_step: Rc<RefCell<Option<CurrentStep>>>,
){
    match
        current_sign
            .borrow()
            .to_owned()
    {
        None =>{
            current_step
                .replace(
                    None
                );
        },
        Some(sign) => match
            sign
                .animation
                .len()
        {
            0 | 1 => {
                current_step
                    .replace(
                        None
                    );
            },
            _ => {
                let mut map_step_to_shape: HashMap<usize, ShapeValue> = HashMap::new();
                let mut steps: usize = 0;
                for
                    (
                        i_shape,
                        animation_shape,
                        )
                in
                    sign
                        .animation
                        .iter()
                        .enumerate()
                {
                    match
                        animation_shape
                            .animation
                            .clone()
                    {
                        None => (),
                        Some(animation_values) => {
                            steps += animation_values
                                .steps
                                .len();
                            map_step_to_shape
                                .extend(
                                    animation_values
                                        .steps
                                        .iter()
                                        .zip(
                                            animation_values
                                                .duration
                                                .iter()
                                        )
                                        .map(
                                            |
                                                (
                                                    step,
                                                    duration,
                                                )
                                            | {
                                                (
                                                    step
                                                        .to_owned(),
                                                    ShapeValue {
                                                        duration: duration.to_owned(),
                                                        shape: i_shape,
                                                    },
                                                )
                                            }
                                        )
                                        .collect::<HashMap<usize, ShapeValue>>()
                                );
                        },
                    };
                };
                current_step
                    .replace(
                        Some(
                            CurrentStep {
                                steps,
                                map_step_to_shape,
                                shape_diff: AnimationShape::default(),
                                shape_start: AnimationShape::default(),
                                ..Default::default()
                            }
                        )
                    );
            },
        },
    };
}
