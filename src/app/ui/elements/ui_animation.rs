
use crate::{
    app::{
        animation::CurrentStep,
        events::keyboard::KeyboardEvent,
        localization::get_string,
    },
    constants::DEFAULT_TOP_PANEL_SPACE,
    dictionary::sign::Sign,
};
use eframe::egui::{
    Context,
    Ui,
};
use fluent::{
    bundle::FluentBundle,
    FluentResource,
};
use intl_memoizer::IntlLangMemoizer;
use std::{
    cell::RefCell,
    rc::Rc,
    time::{
        Duration,
        Instant,
    },
};

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct UiElementAnimation {}

impl UiElementAnimation {
    pub fn buttons(
        context: &Context,
        current_sign: Rc<RefCell<Option<Sign>>>,
        current_step: Rc<RefCell<Option<CurrentStep>>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        ui: &mut Ui,
    ) {
        match
            current_sign
                .borrow()
                .to_owned()
        {
            None => (),
            Some(sign) => match // use app.current_frame ????????????????????????????
                sign
                    .animation
                    .len()
            {
                0 | 1 => (),
                _ => {
                    let step_option: Option<CurrentStep> = current_step
                        .borrow()
                        .to_owned();
                    match
                        step_option
                    {
                        None => (),
                        Some(step) => {
                            ui
                                .vertical_centered_justified(
                                    |ui_button| {
                                        ui_button
                                            .add_space(
                                                DEFAULT_TOP_PANEL_SPACE
                                            );
                                        UiElementAnimation::pause(
                                            context,
                                            current_step
                                                .clone(),
                                            localization
                                                .clone(),
                                            step
                                                .clone(),
                                            ui_button,
                                        );
                                        ui_button
                                            .add_space(
                                                DEFAULT_TOP_PANEL_SPACE
                                            );
                                        UiElementAnimation::restart(
                                            context,
                                            current_step
                                                .clone(),
                                            localization,
                                            step,
                                            ui_button,
                                        );
                                    },
                                );
                        },
                    };
                },
            },
        };
    }

}

impl UiElementAnimation {
    fn pause(
        context: &Context,
        current_step: Rc<RefCell<Option<CurrentStep>>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        step: CurrentStep,
        ui: &mut Ui,
    ) {
        if
            ui
                .button(
                    format!(
                        "{}  [ {} ]",
                        match
                            step
                                .pause
                        {
                            true => "▶",
                            false => "⏸",
                        },
                        get_string(
                            Rc::clone(&localization),
                            "space",
                        ),
                    )
                )
                .clicked()
            |
            // Or press key 'space':
            KeyboardEvent::pause(
                context,
            )
        {
            current_step
                .replace(
                    Some(
                        match
                            step
                                .pause
                        {
                            // Unpause:
                            true => {
                                let time_start: Instant = Instant::now()
                                    -
                                    step
                                        .duration_diff;
                                CurrentStep {
                                    pause: false,
                                    time_start,
                                    ..step
                                }
                            },
                            // Pause:
                            false => {
                                let duration_diff: Duration = step
                                    .time_start
                                    .elapsed();
                                CurrentStep {
                                    pause: true,
                                    duration_diff,
                                    ..step
                                }
                            },
                        }
                    )
                );
        };
    }

    pub fn restart(
        context: &Context,
        current_step: Rc<RefCell<Option<CurrentStep>>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        step: CurrentStep,
        ui: &mut Ui,
    ) {
        if
            ui
                .button(
                    format!(
                        "{}  [ ⬅ ]",
                        get_string(
                            Rc::clone(&localization),
                            "restart",
                        ),
                    )
                )
                .clicked()
            |
            // Or press key 'left':
            KeyboardEvent::restart(
                context,
            )
        {
            // update app.current_frame too !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // println!("ii restart animation");
            current_step
                .replace(
                    Some(
                        CurrentStep {
                            step_current: 0,
                            step_next: 0,
                            step_subframe: 0,
                            time_start: Instant::now(),
                            ..step
                        }
                    )
                );
        };
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
