
use crate::{
    app::{
        events::keyboard::KeyboardEvent,
        localization::get_string,
        options::AppOptions,
    },
    dictionary::{
        sign::Sign,
        sign_language::{
            SignLanguage,
            SIGN_LANGUAGES,
        },
        Dictionary,
    },
};
use eframe::egui::{
    ComboBox,
    Context,
    Ui,
};
use fluent::{
    bundle::FluentBundle,
    FluentResource,
};
use intl_memoizer::IntlLangMemoizer;
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct UiElementDictionary {}

impl UiElementDictionary {
    pub fn select(
        current_sign: Rc<RefCell<Option<Sign>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
        ui: &mut Ui,
    ) {
        let mut app_options: AppOptions = options
            .borrow()
            .to_owned();

        ComboBox::from_id_salt(
            "ComboBox sign language"
        )
            .selected_text(
                app_options
                    .sign_language
                    .to_string(),
            )
            .wrap()
            .show_ui(
                ui,
                |ui_combo| {
                    let sign_language_check: SignLanguage = app_options
                        .sign_language;
                    for
                        sign_language
                    in
                        SIGN_LANGUAGES
                    {
                        ui_combo
                            .selectable_value(
                                &mut app_options
                                    .sign_language,
                                sign_language,
                                sign_language
                                    .to_string(),
                            );
                    };
                    if
                        app_options
                            .sign_language
                        !=
                        sign_language_check
                    {
                        options
                            .replace(
                                app_options
                            );
                        let dictionary_borrow = dictionary
                            .borrow()
                            .to_owned();
                        dictionary_borrow
                            .update(
                                current_sign,
                                dictionary,
                                localization,
                                options,
                            );
                    };
                }
            );
    }

    pub fn load(
        current_sign: Rc<RefCell<Option<Sign>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
        ui: &mut Ui,
    ) {
        if
            ui
                .button(
                    get_string(
                        localization
                            .clone(),
                        "load_file"
                    ),
                )
                .clicked()
        {
            // Dictionary is stored in Dictonary::update().
            let mut dictionary_mut: Dictionary = dictionary
                .borrow()
                .to_owned();
            dictionary_mut
                .load(
                    current_sign,
                    dictionary,
                    localization,
                    options,
                );
        };
    }

    pub fn reload(
        context: &Context,
        current_sign: Rc<RefCell<Option<Sign>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
        ui: &mut Ui,
    ) {
        if
            // Click button:
            ui
                .button(
                    format!(
                        "{}  [ R ]",
                        get_string(
                            localization
                                .clone(),
                            "reload", //"refresh",
                        ),
                    )
                )
                .clicked()
            |
            // Or press key 'R':
            KeyboardEvent::refresh(
                context,
            )
        {
            let dictionary_borrow = dictionary
                .borrow()
                .to_owned();
            if
                dictionary_borrow
                    .path
                    .is_none()
            {
                println!("!! no update for default dictionary");
            };
            dictionary_borrow
                .update(
                    current_sign,
                    dictionary,
                    localization,
                    options,
                );
        };
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
