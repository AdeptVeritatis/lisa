
use crate::{
    app::{
        animation::camera::Camera,
        localization::get_string,
    },
    constants::{
        DEFAULT_ANGLE_XZ,
        DEFAULT_ANGLE_Y,
        DEFAULT_DIST,
        DEFAULT_MOVE_H,
        DEFAULT_MOVE_V,
        MOVE_H_DECIMALS,
        MOVE_H_RANGE,
        MOVE_H_SPEED,
        MOVE_V_DECIMALS,
        MOVE_V_RANGE,
        MOVE_V_SPEED,
    },
};
use eframe::egui::{
    DragValue,
    Grid,
    Ui,
};
use fluent::{
    bundle::FluentBundle,
    FluentResource,
};
use intl_memoizer::IntlLangMemoizer;
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct UiElementView {}

impl UiElementView {
    pub fn set(
        camera: Rc<RefCell<Camera>>,
        ui: &mut Ui,
    ) {
        let mut camera_mut: Camera = camera
            .borrow()
            .to_owned();
        Grid::new(
            "SidePanelMoveCamera"
        )
            .show(
                ui,
                |ui_grid| {
                    ui_grid
                        .label(
                            " ⬅ | ➡" // "⬅️|➡️:"
                            // format!(
                            //     "{}:",
                            //     get_string(
                            //         Rc::clone(&localization),
                            //         "move_h",
                            //     ),
                            // )
                        );
                    ui_grid
                        .add(
                            DragValue::new(&mut camera_mut.move_h)
                                .fixed_decimals(MOVE_H_DECIMALS)
                                .range(MOVE_H_RANGE)
                                .speed(MOVE_H_SPEED),
                        );
                    ui_grid
                        .end_row();
                    ui_grid
                        .label(
                            " ⬇ | ⬆" // "⬇️|⬆️:"
                            // format!(
                            //     "{}:",
                            //     get_string(
                            //         Rc::clone(&localization),
                            //         "move_v",
                            //     ),
                            // )
                        );
                    ui_grid
                        .add(
                            DragValue::new(&mut camera_mut.move_v)
                                .fixed_decimals(MOVE_V_DECIMALS)
                                .range(MOVE_V_RANGE)
                                .speed(MOVE_V_SPEED),
                        );
                    // ui_grid
                    //     .end_row();
                },
            );

        camera
            .replace(
                camera_mut
            );
    }

    pub fn mirror(
        camera: Rc<RefCell<Camera>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        ui: &mut Ui,
    ) {
        let mut camera_mut: Camera = camera
            .borrow()
            .to_owned();
        if
            ui
                .button(
                    get_string(
                        localization,
                        match
                            camera_mut
                                .mirrored
                        {
                            false => "mirror",
                            true => "mirrored",
                        },
                    )
                )
                .clicked()
        {
            camera_mut.mirrored = !camera_mut
                .mirrored;
            camera
                .replace(
                    camera_mut
                );
        };
    }

    pub fn reset(
        camera: Rc<RefCell<Camera>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        ui: &mut Ui,
    ) {
        if
            ui
                .button(
                    get_string(
                        localization,
                        "reset",
                    )
                )
                .clicked()
        {
            let mut camera_mut: Camera = camera
                .borrow()
                .to_owned();
            camera_mut.angle_xz = DEFAULT_ANGLE_XZ;
            camera_mut.angle_y = DEFAULT_ANGLE_Y;
            camera_mut.dist = DEFAULT_DIST;
            camera_mut.move_h = DEFAULT_MOVE_H;
            camera_mut.move_v = DEFAULT_MOVE_V;
            // camera.scale_factor = DEFAULT_SCALE_FACTOR;
            camera
                .replace(
                    camera_mut
                );
        };
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
