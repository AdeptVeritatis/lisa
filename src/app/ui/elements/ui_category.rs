

use crate::{
    app::{
        // localization::get_string,
        options::AppOptions,
    },
    dictionary::{
        categories::Category,
        sign::Sign,
        Dictionary,
    },
};
use eframe::egui::{
    ComboBox,
    // Context,
    Ui,
};
use fluent::{
    bundle::FluentBundle,
    FluentResource,
};
use intl_memoizer::IntlLangMemoizer;
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

// #[derive(Debug, Clone)]
pub struct UiElementCategory {}

impl UiElementCategory {
    pub fn select(
        current_sign: Rc<RefCell<Option<Sign>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
        ui: &mut Ui,
    ) {
        let mut app_options: AppOptions = options
            .borrow()
            .to_owned();

        ComboBox::from_id_salt(
            "ComboBox category"
        )
            .selected_text(
                app_options
                    .category
                    .to_string(),
            )
            .wrap()
            .show_ui(
                ui,
                |ui_combo| {
                    let language = app_options
                        .language
                        .to_string();
                    let category_check: Category = app_options
                        .category
                        .clone();
                    for
                        category
                    in
                        [
                            Category::All(
                                language
                                    .clone()
                            ),
                            Category::Alphabet(
                                language
                                    .clone()
                            ),
                            Category::Color(
                                language
                                    .clone()
                            ),
                            Category::Number(
                                language
                                    .clone()
                            ),
                            Category::Preposition(
                                language
                            ),
                        ]
                        // SIGN_CATEGORIES
                    {
                        ui_combo
                            .selectable_value(
                                &mut app_options
                                    .category,
                                category
                                    .clone(),
                                category
                                    .to_string(),
                            );
                    };
                    if
                        app_options
                            .category
                        !=
                        category_check
                    {
                        options
                            .replace(
                                app_options
                            );
                        let dictionary_borrow = dictionary
                            .borrow()
                            .to_owned();
                        dictionary_borrow
                            .update(
                                current_sign,
                                dictionary,
                                localization,
                                options,
                            );
                    };
                }
            );
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
