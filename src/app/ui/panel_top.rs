
use crate::{
    app::{
        animation::CurrentStep,
        options::AppOptions,
        ui::elements::{
            ui_animation::UiElementAnimation,
            ui_sign::UiElementSign,
        },
    },
    constants::{
        DEFAULT_TOP_PANEL_SPACE,
        DEFAULT_TOP_PANEL_HEIGHT,
    },
    dictionary::{
        sign::Sign,
        Dictionary,
    },
};
use eframe::egui::{
    containers::panel::{
        TopBottomPanel,
        TopBottomSide,
    },
    Align,
    Context,
    Id,
    // Label,
    Layout,
    ScrollArea,
    Ui,
    Vec2,
};
use fluent::{
    bundle::FluentBundle,
    FluentResource,
};
use intl_memoizer::IntlLangMemoizer;
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct UiTopPanel {}

impl UiTopPanel {
    pub fn draw(
        context: &Context,
        current_sign: Rc<RefCell<Option<Sign>>>,
        current_step: Rc<RefCell<Option<CurrentStep>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        filter: Rc<RefCell<String>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
    ) {
        TopBottomPanel::new(
            TopBottomSide::Top,
            Id::new("top"),
        )
            .exact_height(DEFAULT_TOP_PANEL_HEIGHT)
            .show(
                context,
                move |ui_panel| {
                    ui_panel
                        .add_space(DEFAULT_TOP_PANEL_SPACE);
                    // When window width is too small, use scroll bar.
                    ui_panel
                        .horizontal(
                            |ui_horizontal| {
                                const TOP_PANEL_OPTIONS_WIDTH: f32 = 150.0; // move to constants.rs !!!
                                let panel_size: Vec2 = ui_horizontal
                                    .available_size();
                                let panel_size_left: Vec2 = Vec2::new(
                                    (
                                        panel_size
                                            .x
                                        -
                                        TOP_PANEL_OPTIONS_WIDTH
                                        -
                                        8.0 // const !!!
                                    )
                                        .abs(),
                                    DEFAULT_TOP_PANEL_HEIGHT,
                                );
                                let panel_size_right: Vec2 = Vec2::new(
                                    TOP_PANEL_OPTIONS_WIDTH,
                                    DEFAULT_TOP_PANEL_HEIGHT,
                                );
                            // Left side:
                                ui_horizontal
                                    .allocate_ui_with_layout(
                                        panel_size_left,
                                        Layout::top_down_justified(
                                            Align::Center
                                        ),
                                        |ui_left| {
                                            let current_sign_clone = current_sign
                                                .clone();
                                            let current_step_clone = current_step
                                                .clone();
                                            let dictionary_clone = dictionary
                                                .clone();
                                            let filter_clone = filter
                                                .clone();
                                            let localization_clone = localization
                                                .clone();
                                            let options_clone = options
                                                .clone();

                                            ScrollArea::vertical()
                                                .show(
                                                    ui_left,
                                                    move |ui_scroll| {
                                                        UiTopPanel::draw_selector(
                                                            current_sign_clone,
                                                            current_step_clone,
                                                            dictionary_clone,
                                                            filter_clone,
                                                            localization_clone,
                                                            options_clone,
                                                            panel_size_left
                                                                .x,
                                                            ui_scroll,
                                                        );
                                                    }
                                                );
                                        },
                                    );
                            // Right side:
                                ui_horizontal
                                    .allocate_ui_with_layout(
                                        panel_size_right,
                                        Layout::top_down(
                                            Align::Min
                                        ),
                                        |ui_right| {
                                            UiTopPanel::draw_options(
                                                context,
                                                current_sign,
                                                current_step,
                                                dictionary,
                                                filter,
                                                localization,
                                                options,
                                                ui_right,
                                                TOP_PANEL_OPTIONS_WIDTH - 36.0, // const !!!
                                            );
                                        },
                                    );
                            }
                        );
                },
            );
    }

}

impl UiTopPanel {
// Sign selector and translation on the left side:
    fn draw_selector(
        current_sign: Rc<RefCell<Option<Sign>>>,
        current_step: Rc<RefCell<Option<CurrentStep>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        filter: Rc<RefCell<String>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
        panel_width: f32,
        ui: &mut Ui,
    ) {
        UiElementSign::select(
            current_sign
                .clone(),
            current_step,
            dictionary,
            filter,
            localization
                .clone(),
            options
                .clone(),
            panel_width,
            ui,
        );

        ui
            .add_space(6.0); // 16.0

        UiElementSign::translation(
            current_sign,
            localization,
            options,
            ui,
        );
    }

// Options on the right side:
    fn draw_options(
        context: &Context,
        current_sign: Rc<RefCell<Option<Sign>>>,
        current_step: Rc<RefCell<Option<CurrentStep>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        filter: Rc<RefCell<String>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
        ui: &mut Ui,
        width: f32,
    ) {
        UiElementSign::filter(
            filter,
            localization
                .clone(),
            ui,
            width,
        );

        ui
            .add_space(DEFAULT_TOP_PANEL_SPACE); // 6.0, 16.0

        UiElementSign::navigate(
            context,
            current_sign
                .clone(),
            current_step
                .clone(),
            dictionary,
            localization
                .clone(),
            options,
            ui,
        );

        UiElementAnimation::buttons(
            context,
            current_sign,
            current_step,
            localization,
            ui,
        );
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
