#![allow(clippy::derivable_impls)]

pub mod body;
pub mod camera;
pub mod indices;
pub mod render;
pub mod shape;
pub mod skeleton;
pub mod vertices;

use crate::{
    app::animation::shape::AnimationShape,
    dictionary::sign::Sign,
};
use std::{
    collections::HashMap,
    time::{
        Duration,
        Instant,
    },
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, Copy)]
pub enum AnimationSide {
    Left,
    Right,
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, PartialEq)]
pub struct AnimationValues {
    pub duration: Vec<Duration>,
    pub steps: Vec<usize>,
    // pub scale_factor: f32,
}

impl Default for AnimationValues {
    fn default() -> Self {
        Self {
            duration: Vec::new(),
            steps: Vec::new(),
            // scale_factor: 2.0, // use slider !!! move to app ???
}
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct CurrentStep { // ???
    /// Number of Steps.
    // Set in panel_top.rs when new Sign is selected.
    pub steps: usize,

    pub step_current: usize,
    pub step_next: usize,
    /// Subframe counter of a step.
    pub step_subframe: usize,

    pub map_step_to_shape: HashMap<usize, ShapeValue>,
    pub pause: bool,
    pub pos_current: usize,
    pub pos_next: usize,
    pub shape_diff: AnimationShape,
    pub shape_start: AnimationShape,
    pub step_duration: Duration,
    pub duration_diff: Duration,
    pub time_start: Instant,
}

impl Default for CurrentStep {
    fn default() -> Self {
        Self {
            steps: 0,

            step_current: 0,
            step_next: 0,
            step_subframe: 0,

            map_step_to_shape: HashMap::new(),
            pause: false,
            pos_current: 0,
            pos_next: 0,
            shape_diff: AnimationShape::default(),
            shape_start: AnimationShape::default(),
            step_duration: Duration::from_secs(1),
            duration_diff: Duration::from_secs(0),
            time_start: Instant::now(),
        }
    }
}

impl CurrentStep {
    pub fn update_time(
        &mut self,
    ) {
        if
            self
                .step_current
            +
            1
            <
            self
                .steps
        {
            self.time_start = Instant::now();
        };
    }

    pub fn select_next_step(
        &mut self,
    ) {
        if
            self
                .steps
            >
            self
                .step_next
            +
            1

            &&

            !self
                .pause
        {
            self.step_next += 1;
        };
    }

    pub fn get_shapes(
        &mut self,
        sign: Sign,
    ) {
        // Get current and next shape value.
        let current_value: ShapeValue = match
            self
                .map_step_to_shape
                .get(
                    &self
                        .step_current
                )
        {
            None => ShapeValue::default(),
            Some(shape) => shape.to_owned(),
        };
        let next_value: ShapeValue = match
            self
                .map_step_to_shape
                .get(
                    &self
                        .step_next
                )
        {
            None => ShapeValue {
                shape: 1, // ????????????
                ..Default::default()
            },
            Some(shape) => shape.to_owned(),
        };
        // Get shapes.
        let shape_current: AnimationShape = match
            sign
                .animation
                .get(
                    current_value
                        .shape
                )
        {
            None => AnimationShape::default(),
            Some(shape) => shape.to_owned(),
        };
        let shape_next: AnimationShape = match
            sign
                .animation
                .get(
                    next_value
                        .shape
                )
        {
            None => AnimationShape::default(),
            Some(shape) => shape.to_owned(),
        };
        self.shape_start = shape_current
            .clone();
        // Calculate diff shape.
        self.shape_diff = AnimationShape::shape_diff(
            shape_current,
            shape_next,
        );
        // Get duration.
        self.step_duration = current_value
            .duration;
        // Store positions to check for still frames.
        self.pos_current = current_value
            .shape;
        self.pos_next = next_value
            .shape;
    }

}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct ShapeValue {
    pub duration: Duration,
    pub shape: usize,
}

impl Default for ShapeValue {
    fn default() -> Self {
        Self {
            duration: Duration::from_secs(1),
            shape: 0,
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
