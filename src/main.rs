#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

pub mod app;
pub mod constants;
pub mod dictionary;

use crate::{
    app::App,
    constants::APP_NAME,
};
use eframe::{
    NativeOptions,
    Renderer,
};

// ----------------------------------------------------------------------------

fn main() -> Result<(), eframe::Error> {
    let options = NativeOptions {
        depth_buffer: 32, // for Depth32Float
        renderer: Renderer::Wgpu,
        ..Default::default()
    };
    eframe::run_native(
        APP_NAME,
        options,
        Box::new(
            |creation_context| {
                Ok(
                    Box::new(
                        App::new(
                            creation_context,
                        ),
                    ),
                )
            }
        ),
    )
}
