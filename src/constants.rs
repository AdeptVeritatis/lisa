
use eframe::wgpu::TextureFormat;
use std::ops::RangeInclusive;
use unic_langid::{
    langid,
    LanguageIdentifier,
};

// ----------------------------------------------------------------------------

pub const APP_NAME: &str = "LiSA"; // "LinguaSignansApp" "LearnInternationalSignsApp";

// Localization:
pub const DEFAULT_LANGUAGE: LanguageIdentifier = langid!("en-US");
pub const LOCALE_DE_DE: &str = include_str!("app/localization/de-DE.ftl");
pub const LOCALE_EN_US: &str = include_str!("app/localization/en-US.ftl");
pub const LOCALE_FR_FR: &str = include_str!("app/localization/fr-FR.ftl");

// Dictionary:
// pub const DEFAULT_DICTIONARY_DIRECTORY: &str = "assets/"; // "assets/"; // "src/dictionary/";
// pub const DEFAULT_DICTIONARY_FILE: &str = "dictionary.toml";
// pub const DEFAULT_DICTIONARY_PATH: &str = "dictionary/dictionary.toml";
pub const DICTIONARY: &str = include_str!("dictionary/dictionary.toml");
pub const VERSION_PREFIX: &str = "  [ ";
pub const VERSION_SUFFIX: &str = " ]";

// Panel:
pub const DEFAULT_BOTTOM_PANEL_SPACE: f32 = 12.0;
pub const DEFAULT_SIDE_PANEL_SPACE: f32 = 8.0;
pub const SIDE_PANEL_SPACE_NEXT: f32 = 16.0;
pub const DEFAULT_SIDE_PANEL_WIDTH: f32 = 120.0;
pub const DEFAULT_TOP_PANEL_HEIGHT: f32 = 122.0; //100.0; // 120.0;
pub const DEFAULT_TOP_PANEL_SPACE: f32 = 8.0; // 24.0
// pub const MAX_TOP_PANEL_COLUMN_WIDTH: f32 = 160.0;
pub const MIN_TOP_PANEL_COLUMN_WIDTH: f32 = 60.0;

// Render:
pub const DEPTH_FORMAT: TextureFormat = TextureFormat::Depth32Float;
pub const SHADER: &str = include_str!("app/animation/shader.wgsl");

// Vertices:
pub const VERTEX_W: f32 = 1.0;
pub const ARM_LOWER_LENGTH: f32 = 0.07;
pub const ARM_UPPER_LENGTH: f32 = 0.08;
pub const ARM_WIDTH: f32 = 0.004;
const BODY_WIDTH: f32 = 0.040; // 0.5;
pub const FINGER_LENGTH: f32 = 0.006;
pub const FINGER_WIDTH: f32 = 0.0008;
pub const PALM_DEPTH: f32 = FINGER_WIDTH * 3.0;
pub const PALM_LENGTH: f32 = 0.01;
pub const PALM_WIDTH: f32 = 0.005;
pub const FINGER_THUMB_LENGTH_FACTOR: f32 = 1.25;
pub const FINGER_INDEX_LENGTH_FACTOR: f32 = 1.1;
pub const FINGER_MIDDLE_LENGTH_FACTOR: f32 = 1.2;
pub const FINGER_RING_LENGTH_FACTOR: f32 = 1.1;
pub const FINGER_PINKIE_LENGTH_FACTOR: f32 = 0.9;
pub const LIMB_BASE_LENGTH_FACTOR: f32 = 1.0;
pub const LIMB_MIDDLE_LENGTH_FACTOR: f32 = 0.7;
pub const LIMB_TIP_LENGTH_FACTOR: f32 = 0.5;
pub const RECTANGLE_PREV_ANGLE: f32 = 0.0;
// Values x & y from -1.0 to 1.0.
// Value z from 0.0 to 1.0.
// Camera is at [0.0, 0.0, 0.0]. // ???
// Fix camera position later. // ???
pub const CENTER_X: f32 = 0.0;
pub const START_X_L: f32 = CENTER_X + BODY_WIDTH;
pub const START_X_R: f32 = CENTER_X - BODY_WIDTH;
pub const START_Y: f32 = 0.02; // 0.1;
// pub const START_Z: f32 = 0.5; // 0.5; // ???
pub const START_Z: f32 = 0.5; // 0.5; // ???
// pub const SCALE_Z: f32 = 1.0; // 0.1;

// Camera:
pub const CAMERA_DRAG_SPEED: f32 = 0.0005;
pub const CAMERA_SCROLL_SPEED: f32 = 0.00025;
pub const DEFAULT_ANGLE_XZ: f32 = 0.0;
pub const DEFAULT_ANGLE_Y: f32 = 0.0;
pub const DEFAULT_DIST: f32 = 0.25; // std::f32::consts::PI;
pub const DEFAULT_FOV: f32 = std::f32::consts::FRAC_PI_4;
pub const DEFAULT_MOVE_H: f32 = 0.0;
// pub const DEFAULT_Z_FAR: f32 = 2.0;
pub const DEFAULT_Z_NEAR: f32 = 0.01;
pub const MOVE_H_DECIMALS: usize = 3;
pub const MOVE_H_RANGE: RangeInclusive<f32> = RangeInclusive::new(-1.0, 1.0);
pub const MOVE_H_SPEED: f32 = 0.0002;
pub const DEFAULT_MOVE_V: f32 = 0.0;
pub const MOVE_V_DECIMALS: usize = 3;
pub const MOVE_V_RANGE: RangeInclusive<f32> = RangeInclusive::new(-1.0, 1.0);
pub const MOVE_V_SPEED: f32 = 0.0002;

// Colors:
pub const ARM_UPPER_COLOR: usize = 0;
pub const ARM_LOWER_COLOR: usize = 1;
pub const PALM_COLOR: usize = 2;
pub const LIMB_BASE_COLOR: usize = 3;
pub const LIMB_MIDDLE_COLOR: usize = 4;
pub const LIMB_TIP_COLOR: usize = 5;
// Colorblind friendly colors
// Grabbed with kcolorchooser, which may give incorrect results.
pub const COLORS: [[f32; 4]; 8] = [
    [0.898, 0.592, 0.078, 1.0], // dark orange
    [0.286, 0.675, 0.898, 1.0], // bright blue
    [0.000, 0.584, 0.416, 1.0], // dark green
    [0.941, 0.882, 0.255, 1.0], // yellow
    [0.000, 0.408, 0.663, 1.0], // dark blue
    [0.824, 0.337, 0.059, 1.0], // dark red
    [0.780, 0.439, 0.620, 1.0], // violet / purple
    [0.000, 0.000, 0.000, 1.0], // black
];
