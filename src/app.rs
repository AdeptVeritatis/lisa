pub mod animation;
pub mod events;
pub mod localization;
pub mod options;
pub mod ui;

use crate::{
    app::{
        animation::{
            camera::Camera,
            render::prepare_render_pipeline,
            CurrentStep,
        },
        localization::default_locale,
        options::AppOptions,
        ui::UiApp,
    },
    constants::DICTIONARY,
    dictionary::{
        sign::Sign,
        Dictionary,
    },
};
use eframe::{
    egui::{
        Context,
        // ThemePreference,
        // Visuals
    },
    egui_wgpu::{
        wgpu::AdapterInfo,
        RenderState,
    },
    CreationContext,
    Frame,
};
use fluent::{
    bundle::FluentBundle,
    FluentResource,
};
use intl_memoizer::IntlLangMemoizer;
use std::{
    cell::RefCell,
    rc::Rc,
    // sync::Arc,
};

// ----------------------------------------------------------------------------

/// App to learn sign language vocabulary.
#[derive(Clone)]
pub struct App {
    // better method than Rc<RefCell<>> anything individually???
    pub camera: Rc<RefCell<Camera>>,
    pub current_sign: Rc<RefCell<Option<Sign>>>,
    pub current_step: Rc<RefCell<Option<CurrentStep>>>,
    pub dictionary: Rc<RefCell<Dictionary>>,
    // pub dictionary_dir: String, // Path
    // pub dictionary_file: String, // Path
    pub filter: Rc<RefCell<String>>,
    pub localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
    // pub message: Arc<String>,
    pub options: Rc<RefCell<AppOptions>>,
}

impl App {
    pub fn new(
        context: &CreationContext,
    ) -> Self {
        // let scale_factor: f32 = DEFAULT_SCALE_FACTOR;

    // Localization:
        let localization: FluentBundle<FluentResource, IntlLangMemoizer> = default_locale();

    // Message:
        // let message: Arc<String> = Arc::new(
        //     String::new()
        //     // String::from("test")
        // );

    // Camera for render scene:
        let camera: Camera = Camera::default();

    // Context wgpu:
        let wgpu_render_state: &RenderState = context
            .wgpu_render_state
            .as_ref()
            .unwrap();

    // Print info:
        print_adapter_info(
            wgpu_render_state
                .adapter
                .get_info(),
        );

    // Render pipeline:
        prepare_render_pipeline(
            camera,
            wgpu_render_state,
        );

    // Visuals - theme:
        // let visuals: Visuals = match
        //     context
        //         .integration_info
        //         .system_theme
        // {
        //     None => {
        //         Visuals::light() // ???
        //     },
        //     Some(theme) => {
        //         println!(
        //             ".. theme:   {:?}",
        //             theme,
        //         );
        //         theme
        //             .egui_visuals()
        //     },
        // };

    // Options:
        // dbg!(dictionary.table.clone());
        let app_options: AppOptions =
            AppOptions {
                // visuals,
                ..Default::default()
            };

    // Dictionary:
        // let dictionary_dir: String = String::from(DEFAULT_DICTIONARY_DIRECTORY);
        // let dictionary_file: String = String::from(DEFAULT_DICTIONARY_FILE);

        // Load different dictionaries or "courses" from a file menu. // ???
        let dictionary: Dictionary = Dictionary::new(
            app_options
                .clone(),
            String::from(
                DICTIONARY
            ),
            None,
        );

    // Filter:
        let filter: String = String::new();

    // Store values:
        Self {
            camera:
                Rc::new(
                    RefCell::new(
                        camera
                    )
                ),
            current_sign:
                Rc::new(
                    RefCell::new(
                        None
                    )
                ),
            current_step:
                Rc::new(
                    RefCell::new(
                        None
                    )
                ),
            dictionary:
                Rc::new(
                    RefCell::new(
                        dictionary
                    )
                ),
            filter:
                Rc::new(
                    RefCell::new(
                        filter
                    )
                ),
            localization:
                Rc::new(
                    RefCell::new(
                        localization
                    )
                ),
            // message,
            options:
                Rc::new(
                    RefCell::new(
                        app_options
                    )
                ),
        }
    }

}

impl eframe::App for App {
    fn update(
        &mut self,
        context: &Context,
        frame: &mut Frame,
    ) {
        // println!("frame start");
        UiApp::draw_ui(
            self,
            context,
            frame,
        );
        // println!("frame end");
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

fn print_adapter_info(
    adapter_info: AdapterInfo,
) {
    // use localization ???
    println!(
        ".. version: {}",
        env!("CARGO_PKG_VERSION"),
    );
    println!(
        ".. device:  {}",
        adapter_info
            .name,
    );
    println!(
        ".. type:    {:?}",
        adapter_info
            .device_type,
    );
    println!(
        ".. driver:  {} {}",
        adapter_info
            .driver,
        adapter_info
            .driver_info,
    );
    println!(
        ".. backend: {:?}",
        adapter_info
            .backend,
    );
}
