#![allow(clippy::derivable_impls)]

use crate::{
    app::{
        animation::{
            body::{
                AnimationArm,
                AnimationHand,
            },
            shape::AnimationShape,
            AnimationValues,
        },
        localization::get_string,
        options::AppOptions,
    },
    constants::{
        VERSION_PREFIX,
        VERSION_SUFFIX,
    },
    dictionary::{
        categories::Category,
        sign_language::SignLanguage,
        Dictionary,
        append_version,
    },
};
use fluent::{
    bundle::FluentBundle,
    FluentResource,
};
use intl_memoizer::IntlLangMemoizer;
use std::{
    cell::RefCell,
    rc::Rc,
    time::Duration,
};
use toml::{
    // Table,
    Value,
};
use unic_langid::langid;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, PartialEq)]
pub struct Sign {
    pub category: Category,

    pub english: String,
    pub french: String,
    pub german: String,

    pub description: String,
    pub note: String,
    pub number: usize,
    pub version: Option<String>,

    pub animation: Vec<AnimationShape>,
}

impl Default for Sign {
    fn default() -> Self {
        Self {
            category: Category::None,
            english: String::new(),
            french: String::new(),
            german: String::new(),
            description: String::new(),
            note: String::new(),
            number: 0,
            version: None,
            animation: Vec::new(),
        }
    }
}

impl Sign {
    pub fn from_key(
        app_options: AppOptions,
        dictionary: Rc<RefCell<Dictionary>>,
        key: String,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
    ) -> Self {
        let mut sign = Self::default();

        let language: &str = match
            app_options
                .language
                .to_string()
                .as_str()
        {
            "en-US" => "english",
            "fr-FR" => "french",
            "de-DE" => "german",
            _ => "english",
        };

        match
        // same as in dictionary.rs???
        // this breaks after it found key and creates sign
        // rest is same

        // save table content in a BTreeMap with the corresponding key???
            dictionary
                .borrow()
                .table
                .get("dictionary")
        {
            None => {
                println!("!! dictionary not found");
            },
            Some(value) => match
                value
                    .as_array()
            {
                None => {
                    println!("!! dictionary is not an array");
                },
                Some(table) => {
                    for
                        element
                    in
                        table
                    {
                        match
                            element
                                .get(language)
                        {
                            None => {
                                println!("!! dictionary element has no key:\n   {element}");
                            },
                            Some(value) => match
                                value
                                    .as_array()
                            {
                                None => {
                                    println!("!! dictionary element description not an array");
                                },
                                Some(array) => match
                                    array
                                        .first()
                                {
                                    None => {
                                        println!("!! dictionary entry has no name");
                                    },
                                    Some(value) => match
                                        value
                                            .as_str()
                                    {
                                        None => {
                                            println!("!! dictionary entry not a string");
                                        },
                                        Some(string_name) => {
                                            let mut name: String = string_name
                                                .to_string();
                                            name = append_version(
                                                element,
                                                name,
                                            );
                                            if
                                                name
                                                ==
                                                key
                                            {
                                                sign
                                                    .get_values(
                                                        app_options,
                                                        element,
                                                        localization,
                                                    );
                                                for
                                                    (
                                                        number,
                                                        key_check,
                                                    )
                                                in
                                                    dictionary
                                                        .borrow()
                                                        .list
                                                        .iter()
                                                        .enumerate()
                                                {
                                                    if
                                                        &key
                                                        ==
                                                        key_check
                                                    {
                                                        sign.number = number;
                                                        break
                                                    }
                                                };
                                                break
                                            };
                                        },
                                    },
                                },
                            },
                        };
                    };
                },
            },
        };
        sign
    }

    fn get_values(
        &mut self,
        app_options: AppOptions,
        element: &Value,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
    ) {
        match
            element
                .get("category")
        {
            None => {
                println!("!! dictionary element has no category entry");
            },
            Some(value) => match
                value
                    .as_str()
            {
                None => {
                    println!("!! dictionary element category not a string");
                },
                // use function and move to dictionary/categories.rs!!!!!!!!!!!
                Some(string) => match
                    string
                {
                    "none" => {
                        self.category = Category::None; // already none
                    },
                    "alphabet"
                    |
                    "letter"
                    => {
                        self.category = Category::Alphabet(
                            app_options
                                .language
                                .to_string()
                        );
                        self.description = self
                            .category
                            .to_string();
                    },
                    "color" => {
                        self.category = Category::Color(
                            app_options
                                .language
                                .to_string()
                        );
                        self.description = self
                            .category
                            .to_string();
                    },
                    "example" => {
                        self.category = Category::Example(
                            app_options
                                .language
                                .to_string()
                        );
                        self.description = self
                            .category
                            .to_string();
                    },
                    "number" => {
                        self.category = Category::Number(
                            app_options
                                .language
                                .to_string()
                        );
                        self.description = self
                            .category
                            .to_string();
                    },
                    "sign" => {
                        self.category = Category::Sign(
                            app_options
                                .language
                                .to_string()
                        );
                        // self.description = self
                        //     .category
                        //     .to_string();
                    },
                    _ => {},
                },
            },
        };
        match
            element
                .get("english")
        {
            None => {
                println!("!! dictionary element has no english entry");
            },
            Some(value) => match
                value
                    .as_array()
            {
                None => {
                    println!("!! dictionary element description not an array");
                },
                Some(array) => {
                    for
                        (
                            num,
                            value,
                        )
                    in
                        array
                            .iter()
                            .enumerate()
                    {
                        match
                            value
                                .as_str()
                        {
                            None => {
                                println!("!! english entry is not a string");
                            },
                            Some(string) => match
                                num
                            {
                                0 => {
                                    self.english = String::from(string);
                                },
                                1 => if
                                    app_options
                                        .language
                                    ==
                                    langid!("en-US")
                                {
                                    if
                                        !string
                                            .is_empty()
                                    {
                                        self.description = String::from(string);
                                    };
                                },
                                2 => if
                                    app_options
                                        .language
                                    ==
                                    langid!("en-US")
                                {
                                    self.note = String::from(string);
                                },
                                _ => {
                                    println!("!! dictionary element with unknown description:\n   value: {value}");
                                },
                            },
                        };
                    };
                },
            },
        };
        match
            element
                .get("french")
        {
            None => {
                println!("!! dictionary element has no french entry");
            },
            Some(value) => match
                value
                    .as_array()
            {
                None => {
                    println!("!! dictionary element version not an array");
                },
                Some(array) => {
                    for
                        (
                            num,
                            value,
                        )
                    in
                        array
                            .iter()
                            .enumerate()
                    {
                        match
                            value
                                .as_str()
                        {
                            None => {
                                println!("!! french entry is not a string");
                            },
                            Some(string) => match
                                num
                            {
                                0 => {
                                    self.french = String::from(string);
                                },
                                1 => if
                                    app_options
                                        .language
                                    ==
                                    langid!("fr-FR")
                                {
                                    if
                                        !string
                                            .is_empty()
                                    {
                                        self.description = String::from(string);
                                    };
                                },
                                2 => if
                                    app_options
                                        .language
                                    ==
                                    langid!("fr-FR")
                                {
                                    self.note = String::from(string);
                                },
                                _ => {
                                    println!("!! dictionary element with unknown description:\n   value: {value}");
                                },
                            },
                        };
                    };
                },
            },
        };
        match
            element
                .get("german")
        {
            None => {
                println!("!! dictionary element has no german entry");
            },
            Some(value) => match
                value
                    .as_array()
            {
                None => {
                    println!("!! dictionary element description not an array");
                },
                Some(array) => {
                    for
                        (
                            num,
                            value,
                        )
                    in
                        array
                            .iter()
                            .enumerate()
                    {
                        match
                            value
                                .as_str()
                        {
                            None => {
                                println!("!! german entry is not a string");
                            },
                            Some(string) => match
                                num
                            {
                                0 => {
                                    self.german = String::from(string);
                                },
                                1 => if
                                    app_options
                                        .language
                                    ==
                                    langid!("de-DE")
                                {
                                    if
                                        !string
                                            .is_empty()
                                    {
                                        self.description = String::from(string);
                                    };
                                },
                                2 => if
                                    app_options
                                        .language
                                    ==
                                    langid!("de-DE")
                                {
                                    self.note = String::from(string);
                                },
                                _ => {
                                    println!("!! dictionary element with unknown description:\n   value: {value}");
                                },
                            },
                        };
                    };
                },
            },
        };
        match
            element
                .get("version")
        {
            None => {
                // println!("!! dictionary element has no version entry");
            },
            Some(value) => match
                value
                    .as_str()
            {
                None => {
                    println!("!! dictionary element description not a string");
                },
                Some(string) => {
                    let mut version: String = String::from(VERSION_PREFIX);
                    version
                        .push_str(
                            string
                        );
                    version
                        .push_str(VERSION_SUFFIX);
                    self.version = Some(
                        version
                    );
                },
            },
        };
        let sign_language: &str = match
            app_options
                .sign_language
        {
            SignLanguage::ASL => "asl", // consts!!!
            SignLanguage::DGS => "dgs",
            SignLanguage::LSF => "lsf",
        };
        match
            // get data depending on app.options.sign_language !!!
            element
                .get(
                    sign_language,
                )
        {
            None => {
                let element_name: String = match
                    app_options
                        .language
                        .to_string()
                        .as_str()
                {
                    "en-US" => self
                        .english
                        .clone(),
                    "fr-FR" => self
                        .french
                        .clone(),
                    "de-DE" => self
                        .german
                        .clone(),
                    _ => self
                        .english
                        .clone(),
                };

                println!(
                    "!! dictionary element '{}' has no entry for {}",
                    element_name,
                    app_options
                        .sign_language,
                );
                self.note = get_string(
                    localization,
                    "sign_not_available",
                );
            },
            Some(lsf_value) => match
                lsf_value
                    .as_array()
            {
                None => {
                    println!("!! LSF entry is not an array");
                },
                Some(lsf_array) => {
                    self.animation = get_animation(
                        self
                            .animation
                            .clone(),
                        lsf_array,
                    );
                },
            },
        };
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

fn get_animation(
    mut animation: Vec<AnimationShape>,
    lsf_array: &Vec<Value>,
) -> Vec<AnimationShape> {
    for
        shape_data
    in
        lsf_array
    {
        let mut arm_left: AnimationArm = AnimationArm::default();
        let mut arm_right: AnimationArm = AnimationArm::default();
        let mut hand_left: AnimationHand = AnimationHand::default();
        let mut hand_right: AnimationHand = AnimationHand::default();
        let mut animation_values: AnimationValues = AnimationValues::default();

        match
            shape_data
                .as_table()
        {
            None => {
                println!("!! shape entry is not a table");
            },
            Some(shape_table) => {
                for
                    (
                        shape_key,
                        shape_value,
                    )
                in
                    shape_table
                        .iter()
                {
                    match
                        shape_key
                            .as_str()
                    {
                        "left" => match
                            shape_value
                                .as_table()
                        {
                            None => {
                                println!("!! left side not a table");
                            },
                            Some(value_left) => {
                                match
                                    value_left
                                        .get("arm")
                                {
                                    None => {
                                        println!("!! left arm not in table");
                                    },
                                    Some(left_arm_value) => {
                                        arm_left = get_arm(
                                            arm_left,
                                            left_arm_value,
                                        );
                                    },
                                };
                                match
                                    value_left
                                        .get("hand")
                                {
                                    None => {
                                        println!("!! left hand not in table");
                                    },
                                    Some(left_hand_value) => {
                                        hand_left = get_hand(
                                            hand_left,
                                            left_hand_value,
                                        );
                                    },
                                };
                            },
                        },
                        "right" => match
                            shape_value
                                .as_table()
                        {
                            None => {
                                println!("!! right side not a table");
                            },
                            Some(value_right) => {
                                match
                                    value_right
                                        .get("arm")
                                {
                                    None => {
                                        println!("!! right arm not in table");
                                    },
                                    Some(right_arm_value) => {
                                        arm_right = get_arm(
                                            arm_right,
                                            right_arm_value,
                                        );
                                    },
                                };
                                match
                                    value_right
                                        .get("hand")
                                {
                                    None => {
                                        println!("!! right hand not in table");
                                    },
                                    Some(right_hand_value) => {
                                        hand_right = get_hand(
                                            hand_right,
                                            right_hand_value,
                                        );
                                    },
                                };
                            },
                        },
                        "duration" => match
                            shape_value
                                .as_array()
                        {
                            None => {
                                println!("!! duration not an array");
                            },
                            Some(duration_list) => {
                                for
                                    duration_value
                                in
                                    duration_list
                                {
                                    match
                                        duration_value
                                            .as_float()
                                    {
                                        None => {
                                            println!("!! duration not a float");
                                        },
                                        Some(value_dur) => {
                                            animation_values
                                                .duration
                                                .push(
                                                    Duration::from_millis((value_dur * 1000.0) as u64)
                                                );
                                        },
                                    };
                                };
                            },
                        },
                        "step" => match
                            shape_value
                                .as_array()
                        {
                            None => {
                                println!("!! frame not an array");
                            },
                            Some(frame_list) => {
                                for
                                    frame_value
                                in
                                    frame_list
                                {
                                    match
                                        frame_value
                                            .as_integer()
                                    {
                                        None => {
                                            println!("!! frame not an integer");
                                        },
                                        Some(value_frame) => {
                                            animation_values
                                                .steps
                                                .push(
                                                    value_frame as usize
                                                );
                                        },
                                    };
                                };
                            },
                        },
                        _ => {
                            println!("!! key \"{shape_key}\" not found");
                        },
                    };
                };
            },
        };
        animation
            .push(
                AnimationShape {
                    arm_left,
                    arm_right,
                    hand_left,
                    hand_right,
                    animation: match
                        animation_values
                            .duration
                            .is_empty()
                    {
                        false => Some(animation_values),
                        true => match
                            animation_values
                                .steps
                                .is_empty()
                        {
                            true => None,
                            false => Some(animation_values),
                        },
                    },
                }
            );
    };
    animation
}

// ----------------------------------------------------------------------------

fn get_arm(
    mut arm: AnimationArm,
    value: &Value,
) -> AnimationArm {
    match
        value
            .as_array()
    {
        None => {
            println!("!! arm not an array");
        },
        Some(arm_array) => {
            for
                arm_element
            in
                arm_array
            {
                match
                    arm_element
                        .as_table()
                {
                    None => {
                        println!("!! arm element is not a table");
                    },
                    Some(arm_table) => {
                        for
                            (arm_key, arm_value)
                        in
                            arm_table
                        {
                            match
                                arm_key
                                    .as_str()
                            {
                                "forearm" => match
                                    arm_value
                                        .as_array()
                                {
                                    None => {
                                        println!("!! forearm is not an array");
                                    },
                                    Some(forearm_array) => {
                                        arm
                                            .get_lower_arm(
                                                forearm_array,
                                            );
                                    },
                                },
                                "upperarm" => match
                                    arm_value
                                        .as_array()
                                {
                                    None => {
                                        println!("!! upperarm is not an array");
                                    },
                                    Some(upperarm_array) => {
                                        arm
                                            .get_upper_arm(
                                                upperarm_array,
                                            );
                                    },
                                },
                                _ => {
                                    println!("!! key not in arm table");
                                },
                            }
                        };
                    },
                };
            };
        },
    };
    arm
}

// ----------------------------------------------------------------------------

fn get_hand(
    mut hand: AnimationHand,
    value: &Value,
) -> AnimationHand {
    match
        value
            .as_array()
    {
        None => {
            println!("!! hand not an array");
        },
        Some(hand_array) => {
            for
                hand_element
            in
                hand_array
            {
                match
                    hand_element
                        .as_table()
                {
                    None => {
                        println!("!! hand element is not a table");
                    },
                    Some(hand_table) => {
                        for
                            (hand_key, hand_value)
                        in
                            hand_table
                        {
                            match
                                hand_key
                                    .as_str()
                            {
                                "palm" => match
                                    hand_value
                                        .as_array()
                                {
                                    None => {
                                        println!("!! palm is not an array");
                                    },
                                    Some(palm_array) => {
                                        hand
                                            .get_palm(
                                                palm_array,
                                            );
                                    },
                                },
                                "thumb" => match
                                    hand_value
                                        .as_array()
                                {
                                    None => {
                                        println!("!! thumb is not an array");
                                    },
                                    Some(thumb_array) => {
                                        hand
                                            .thumb
                                            .get_finger(
                                                thumb_array,
                                            );
                                    },
                                },
                                "index" => match
                                    hand_value
                                        .as_array()
                                {
                                    None => {
                                        println!("!! index is not an array");
                                    },
                                    Some(index_array) => {
                                        hand
                                            .index
                                            .get_finger(
                                                index_array,
                                            );
                                    },
                                },
                                "middle" => match
                                    hand_value
                                        .as_array()
                                {
                                    None => {
                                        println!("!! middle is not an array");
                                    },
                                    Some(middle_array) => {
                                        hand
                                            .middle
                                            .get_finger(
                                                middle_array,
                                            );
                                    },
                                },
                                "ring" => match
                                    hand_value
                                        .as_array()
                                {
                                    None => {
                                        println!("!! ring is not an array");
                                    },
                                    Some(ring_array) => {
                                        hand
                                            .ring
                                            .get_finger(
                                                ring_array,
                                            );
                                    },
                                },
                                "pinkie" => match
                                    hand_value
                                        .as_array()
                                {
                                    None => {
                                        println!("!! pinkie is not an array");
                                    },
                                    Some(pinkie_array) => {
                                        hand
                                            .pinkie
                                            .get_finger(
                                                pinkie_array,
                                            );
                                    },
                                },
                                _ => {
                                    println!("!! key not in hand table");
                                },
                            }
                        };
                    },
                };
            };
        },
    };

    hand
}
