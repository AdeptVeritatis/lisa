
// use fluent::{
//     bundle::FluentBundle,
//     FluentResource,
// };
// use intl_memoizer::IntlLangMemoizer;
use std::{
    // cell::RefCell,
    fmt,
    // rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, PartialEq)]
pub enum Category {
    None,
    All(
        // Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>
        String
    ),
    Alphabet(String),
    Color(String),
    Example(String),
    Number(String),
    Preposition(String),
    Sign(String),
}

// pub const SIGN_CATEGORIES: [Category; 3] = [
//     Category::Alphabet(),
//     Category::Number(),
//     Category::Sign(),
// ];

impl fmt::Display for Category {
    // use function? with localization???
    // translate it before or later?????????????????????????
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        match
            // *self
            self
        {
            Self::None => write!(f, "none"),
            // match
            //     language
            //         .as_str()
            // {
            //     "en-US" => write!(f, "none"),
            //     "fr-FR" => write!(f, "vide"),
            //     "de-DE" => write!(f, "leer"),
            //     _ => write!(f, "none")
            // },
            Self::All(language) => match
                language
                    .as_str()
            {
                "en-US" => write!(f, "--- all ---"),
                "fr-FR" => write!(f, "--- tout ---"),
                "de-DE" => write!(f, "--- alle ---"),
                _ => write!(f, "-- all ---")
            },
            Self::Alphabet(language) => match
                language
                    .as_str()
            {
                "en-US" => write!(f, "letter"),
                "fr-FR" => write!(f, "lettre"),
                "de-DE" => write!(f, "Buchstabe"),
                _ => write!(f, "letter")
            },
            Self::Color(language) => match
                language
                    .as_str()
            {
                "en-US" => write!(f, "color"),
                "fr-FR" => write!(f, "couleur"),
                "de-DE" => write!(f, "Farbe"),
                _ => write!(f, "color")
            },
            Self::Example(language) => match
                language
                    .as_str()
            {
                // "en-US" => write!(f, "example"),
                // "fr-FR" => write!(f, "exemple"),
                // "de-DE" => write!(f, "Beispiel"),
                "en-US" => write!(f, "for test purposes"),
                "fr-FR" => write!(f, "à des fins de tests"),
                "de-DE" => write!(f, "zu Testzwecken"),
                _ => write!(f, "example")
            },
            Self::Number(language) => match
                language
                    .as_str()
            {
                "en-US" => write!(f, "number"),
                "fr-FR" => write!(f, "nombre"),
                "de-DE" => write!(f, "Zahl"),
                _ => write!(f, "number")
            },
            Self::Preposition(language) => match
                language
                    .as_str()
            {
                "en-US" => write!(f, "preposition"),
                "fr-FR" => write!(f, "préposition"),
                "de-DE" => write!(f, "Präposition"),
                _ => write!(f, "number")
            },
            Self::Sign(language) => match
                language
                    .as_str()
            {
                "en-US" => write!(f, "sign"),
                "fr-FR" => write!(f, "signe"),
                "de-DE" => write!(f, "Gebärde"),
                _ => write!(f, "sign")
            },
        }
    }
}

impl Category {
    pub fn translate(
        // &mut self, // ???
        &self,
        language: String,
    ) -> Self {
        match
            self
        {
            Category::None => Self::None,
            Category::All(_) => Category::All(language),
            Category::Alphabet(_) => Category::Alphabet(language),
            Category::Color(_) => Category::Color(language),
            Category::Example(_) => Category::Example(language),
            Category::Number(_) => Category::Number(language),
            Category::Preposition(_) => Category::Preposition(language),
            Category::Sign(_) => Category::Sign(language),
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
