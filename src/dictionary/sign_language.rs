
use std::fmt;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum SignLanguage {
    ASL,
    DGS,
    LSF,
}

pub const SIGN_LANGUAGES: [SignLanguage; 3] = [
    SignLanguage::ASL,
    SignLanguage::DGS,
    SignLanguage::LSF,
];

impl fmt::Display for SignLanguage {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        match *self {
            Self::ASL => write!(f, "ASL  -  us"), // "[ 🇺🇸 ] ASL"),
            Self::DGS => write!(f, "DGS  -  de"), // "[ 🇩🇪 ] DGS"),
            Self::LSF => write!(f, "LSF  -  fr"), // "[ 🇫🇷 ] LSF"),
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
