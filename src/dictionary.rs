pub mod categories;
pub mod sign;
pub mod sign_language;

use crate::{
    app::{
        localization::get_string,
        options::AppOptions,
    },
    constants::{
        APP_NAME,
        DICTIONARY,
        VERSION_PREFIX,
        VERSION_SUFFIX,
    },
    dictionary::{
        categories::Category,
        sign::Sign,
    },
};
use fluent::{
    bundle::FluentBundle,
    FluentResource,
};
use intl_memoizer::IntlLangMemoizer;
use rfd::FileDialog;
use std::{
    cell::RefCell,
    fs,
    path::PathBuf,
    rc::Rc,
};
use toml::{
    Table,
    Value,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct Dictionary {
    // List of all known words.
    pub list: Vec<String>,
    pub path: Option<PathBuf>,
    // Table containing parsed data.
    pub table: Table,
}

impl Dictionary {
    pub fn new(
        app_options: AppOptions,
        data: String,
        path: Option<PathBuf>,
    ) -> Self {
        let mut list: Vec<String> = Vec::new();

        let language: &str = match
            app_options
                .language
                .to_string()
                .as_str()
        {
            "en-US" => "english",
            "fr-FR" => "french",
            "de-DE" => "german",
            _ => "english",
        };

        // look at how config files are stored and loaded???

        let table: Table = match
            data
                .parse::<Table>()
        {
            Err(error) => {
                println!("!! failed to load dictionary: {path:?}");
                println!("!! error: {error}");
                Table::new()
            },
            Ok(table) => table,
        };
        // dbg!(table.clone());

        match
        // same as in sign.rs???
        // this scans whole dictionary to create full list
        // rest is same
            table
                .get("dictionary")
        {
            None => {
                println!("!! dictionary not found");
            },
            Some(value) => match
                value
                    .as_array()
            {
                None => {
                    println!("!! dictionary is not an array");
                },
                Some(dictionary) => {
                    for
                        element
                    in
                        dictionary
                    {
                        // dbg!(element.clone());
                        match
                            element
                                .get(
                                    "category"
                                )
                        {
                            None => {
                                println!("!! dictionary element has no key:\n   {element}");
                            },
                            Some(value) => match
                                value
                                    .as_str()
                            {
                                None => {
                                    println!("!! dictionary entry not a string");
                                },
                                // move to function in dictionary/categories.rs??????????????????
                                Some(string_category) => match
                                    app_options
                                        .category
                                {
                                    Category::None => (),
                                    Category::All(_) => {
                                        list = add_element_to_list(
                                            element,
                                            language,
                                            list,
                                        );
                                    },
                                    Category::Alphabet(_) => {
                                        if
                                            string_category
                                            ==
                                            "letter"
                                        {
                                            list = add_element_to_list(
                                                element,
                                                language,
                                                list,
                                            );
                                        };
                                    },
                                    Category::Color(_) => {
                                        if
                                            string_category
                                            ==
                                            "color"
                                        {
                                            list = add_element_to_list(
                                                element,
                                                language,
                                                list,
                                            );
                                        };
                                    },
                                    Category::Example(_) => {},
                                    Category::Number(_) => {
                                        if
                                            string_category
                                            ==
                                            "number"
                                        {
                                            list = add_element_to_list(
                                                element,
                                                language,
                                                list,
                                            );
                                        };
                                    },
                                    Category::Preposition(_) => {
                                        if
                                            string_category
                                            ==
                                            "preposition"
                                        {
                                            list = add_element_to_list(
                                                element,
                                                language,
                                                list,
                                            );
                                        };
                                    },
                                    Category::Sign(_) => {if
                                            string_category
                                            ==
                                            "sign"
                                        {
                                            list = add_element_to_list(
                                                element,
                                                language,
                                                list,
                                            );
                                        };
                                    },
                                },
                            },
                        };
                    };
                },
            },
        };
        list
            // .sort();
            .sort_by(
                |a, b| a
                    .to_lowercase()
                    .cmp(
                        &b
                            .to_lowercase()
                    )
            );
        Self {
            list,
            path,
            table,
        }
    }

    pub fn load(
        &mut self, // ???
        current_sign: Rc<RefCell<Option<Sign>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
    ) {
        match
            FileDialog::new()
                .set_title(
                    format!(
                        "{} - {}",
                        get_string(
                            Rc::clone(&localization),
                            "open_file",
                        ),
                        APP_NAME,
                    )
                )
                .add_filter(
                    "toml",
                    &["toml"],
                )
                .add_filter(
                    "all",
                    &["*"],
                )
                // .add_filter("none", &[])
                .pick_files()
        {
            None => {
                println!("!! no file selected");
            },
            Some(paths) => match
                paths
                    .first()
            {
                None => {
                    println!("!! error: no path for selected file");
                },
                Some(path) => {
                    self.path = Some(
                        path
                            .to_owned()
                    );
                    self
                        .update(
                            current_sign,
                            dictionary,
                            localization,
                            options,
                        );
                },
            },
        }
    }

    pub fn update(
        &self,
        current_sign: Rc<RefCell<Option<Sign>>>,
        dictionary: Rc<RefCell<Dictionary>>,
        localization: Rc<RefCell<FluentBundle<FluentResource, IntlLangMemoizer>>>,
        options: Rc<RefCell<AppOptions>>,
    ) {
        let app_options: AppOptions = options
            .borrow()
            .to_owned();

        // Update dictionary:
        match
            self
                .path
                .clone()
        {
            None => {
                dictionary
                    .replace(
                        Self::new(
                            app_options
                                .clone(),
                            DICTIONARY
                                .to_string(),
                            None,
                        )
                    );
            },
            Some(path) => {
                match
                    fs::read_to_string(
                        path
                            .clone(),
                    )
                {
                    Err(error) => {
                        println!("!! dictionary file not readable: {error}");
                    },
                    Ok(data) => {
                        println!("ii dictionary updated");
                        dictionary
                            .replace(
                                Self::new(
                                    app_options
                                        .clone(),
                                    data,
                                    Some(
                                        path
                                            .to_owned()
                                    ),
                                )
                            );
                    },
                }
            },
        };
        // Update current sign:
        app_options
            .set_sign_language(
                current_sign,
                dictionary,
                localization,
            );
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

fn add_element_to_list(
    element: &Value,
    language: &str,
    mut list: Vec<String>,
) -> Vec<String> {
    match
        element
            .get(
                language
            )
    {
        None => {
            println!("!! dictionary element has no key:\n   {element}");
        },
        Some(value) => match
            value
                .as_array()
        {
            None => {
                println!("!! dictionary element description not an array");
            },
            Some(array) => match
                array
                    .first()
            {
                None => {
                    println!("!! dictionary entry has no name");
                },
                Some(value) => match
                    value
                        .as_str()
                {
                    None => {
                        println!("!! dictionary entry not a string");
                    },
                    Some(string_name) => {
                        let mut name = string_name
                            .to_string();
                        name = append_version(
                            element,
                            name,
                        );
                        list
                            .push(
                                name
                            );
                    },
                },
            },
        },
    };
    list
}

pub fn append_version( // remove pub later
    element: &Value,
    mut name: String,
) -> String {
    match
        element
            .get("version")
    {
        None => {
            // println!("!! dictionary entry has no version");
        },
        Some(value) => match
            value
                .as_str()
        {
            None => {
                println!("!! dictionary value not a string");
            },
            Some(string_version) => {
                name
                    .push_str(
                        VERSION_PREFIX
                    );
                name
                    .push_str(
                        string_version
                    );
                name
                    .push_str(
                        VERSION_SUFFIX
                    );
            },
        },
    };

    name
}
