<div align="center">
    <h1>LiSA</h1>
<h4> A sign language learning app. </h4>

![screenshot](assets/LiSA-screenshot-01.png)

</div>


# Description
`LiSA` is a sign language learning app.

The signs are based on 3D animations, not on photos or videos.

Translations are available in English, French and German.\
The frontend has some optimizations to create and extend the sign language data sets.

It is written in `Rust` using `eframe` and `wgpu`. It is platform independent.

***

The basic functionality works and the body model is pretty solid after the third iteration now.\
My focus was on learning french sign language. The commercial available software did not suit my needs.\
I also wanted to write an app using WebGPU to learn it. So I combined both.\
The main problem is building a huge set of sign animations.

The body model contains arms, hands and fingers. Mouth or other body parts may be added later.\
The colors of the limbs are colorblind friendly.\
Simple rectangular surfaces are used for the 3D animation. A more detailed model can be added later.\
Lights and shadows are on the list, too.

Other sign languages can easily be added.

# Requirements

...

# Installation
A compiled binary will only be available for new versions! This may change with a working CI pipeline.

A compiled binary for Linux is available in [./target/release/](https://gitlab.freedesktop.org/AdeptVeritatis/lisa/-/blob/main/target/release/li_s_a).

Don't forget to make it executable, if necessary.

<!-- A compiled [li_s_a.exe](https://gitlab.freedesktop.org/AdeptVeritatis/lisa/-/blob/main/target/x86_64-pc-windows-gnu/release/li_s_a.exe) for Windows is available, too. -->
A compiled binary for Windows has been available. The Windows build works, but because of some errors showing up in Wine, it will only be released again, when this is solved.

## Building from source
To build `LiSA`, you will need to have `Rust` installed. The recommended way to install `Rust` is from the [official download page](https://www.rust-lang.org/tools/install), using `rustup`.\
For Linux just install `rustup` from the official repositories of your distribution using the package manager.

Minimum supported Rust version: 1.75.0 (use `rustup update`)\
See [docs/msrv.txt](https://gitlab.freedesktop.org/AdeptVeritatis/lisa/-/blob/main/docs/msrv.txt?ref_type=heads) for a detailed list.

Debian Bookworm (stable): rustc 1.63.0, does not work

Clone the main branch.

```
git clone https://gitlab.freedesktop.org/AdeptVeritatis/lisa.git/
cd lisa
cargo build --release
./target/release/li_s_a
```

Once cloned, you only need to `git pull` from the directory to update the source code.\
You can also build and start it with `cargo run --release` afterwards.

## Cross-compiling from Linux to Windows:

```
...
rustup target list
rustup target add x86_64-pc-windows-gnu
cargo build --release --target x86_64-pc-windows-gnu
```

App is in `./target/x86_64-pc-windows-gnu/release/li_s_a.exe`.

# Usage
## Getting started:
Start `LiSA`.

Select a language from the menu.

Select a sign.

Change view settings. (Drag a value to change it.)

Hold the left mouse button and drag it to turn the 3D model.\
Mouse wheel changes distance to the model.

![screenshot](assets/LiSA-screenshot-02.png)

## Dictionary editing:
The default dictionary is compiled into the binary. But you can read from an external file.\
The [dictionary.toml](https://gitlab.freedesktop.org/AdeptVeritatis/lisa/-/blob/main/src/dictionary/dictionary.toml) file is in `src/dictionary/`.\
You only need to load it once and then reload it with a button or key press.\
That makes working on dictionary entries easier and pretty fast.

Load a dictionary.toml-file.

Edit the file, while the app still runs.

Press `reload` to refresh the directory from the file after every change.\
Or press `R` on the keyboard, when the app is in focus.

# Known shortcomings
## Severe:
Windows build creates many messages in Wine and stalls sometimes.

## Handling:
...

## Animation:
...

# Roadmap
Shadows.\
Better 3D model.

Sorting option for sign lists (alphabetical, random).\
Categories for sign lists.\
"I know that sign" mask and filter system.

Persistency for options.\
Loading personal profiles.

Maybe you have a nice idea or need something really urgent.

# Support and contributing
As I am an amateur in publishing my software, I am not familiar with all the habits.

But I am trying to be open for your remarks and wishes. And the issues tracker.

Most welcome are contributions to the sign language data sets.\
Does not matter for which sign language.\
Even looking through existing signs and check their correctness is very helpful.

<!-- Matrix channel: [#LiSA:tchncs.de](https://matrix.to/#/#LiSA:tchncs.de) -->

Matrix channel: [#li_s_a:tchncs.de](https://app.element.io/#/room/#li_s_a:tchncs.de)

You can contact me on Mastodon [@li_s_a@fosstodon.org](https://fosstodon.org/@li_s_a).\
Or just open an issue. I'm fine with that, too.

# Naming
If you have any problem with any names or terms I used for whatever reason, please contact me.

## Redistribution
This is free and open source software. As long as you adhere to the license, you can do whatever you want with it.

You don't like the name? Clone it and rename it!

You don't like, how I handle the project? Clone it and continue as you like!

You like the idea but want to implement it differently? Copy the idea!

You want to see a Flatpak repository of this? Do it yourself!

You want to sell this app? Stick to the license (especially the redistribution of modifications)!

The sign language data can be used in other apps.

# Acknowledgments
Thanks to:

seejayer, for pointing me to many things.

[egui](https://crates.io/crates/egui)

[fluent](https://crates.io/crates/fluent)

[rfd](https://crates.io/crates/rfd)

[serde](https://crates.io/crates/serde)

[toml](https://crates.io/crates/toml)

[wgpu](https://crates.io/crates/wgpu)

[The Rust Programming Language](https://doc.rust-lang.org/book/title-page.html) and the [docs](https://doc.rust-lang.org/stable/std/index.html)

and many more

# Project status
Still motivated.
